<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace'=>'Api'], function () {
    
    Route::post('member/register', 'MemberController@register');
    Route::post('user/login', 'UserController@authenticate');
    Route::post('transactions/sale', 'SaleController@sell');

    Route::group(['middleware' => 'jwt-auth'], function () {
        Route::get('users',function(Request $request){
              return ['response'=>'Joel EYamu'];
          });  
    	Route::post('member/suscription', 'MemberController@assignMemberPackage');
        //sales
        Route::get('transactions/sales', 'SaleController@show');
        Route::get('transactions/sales/bonuses', 'SaleController@showBonuses');

         //Products
        Route::post('products/add', 'ProductController@store');
        Route::delete('products/delete/{id}', 'ProductController@destroy');
        Route::put('products/update/{id}', 'ProductController@update');
    });
});
