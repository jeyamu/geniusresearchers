<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::group(['middleware' => 'guest'], function () { 
          /*Handle login get and post requests*/
         Route::get('/', ['as' => 'login', 
                                  'uses' => 'WelcomeController@login'
                                  ]);
        
        Route::post('/login', ['as' => 'login.auth', 
                                  'uses' => 'UserController@authenticate'
                                  ]);
        // Password reset link request routes...
        Route::get('password/email', 'Auth\PasswordController@getEmail')->name('password.email');
        Route::post('password/email', 'Auth\PasswordController@postEmail');
        // Password reset routes...
        Route::get('password/reset/{token}', 'Auth\PasswordController@getReset')->name('password.reset');
        Route::post('password/reset', 'Auth\PasswordController@postReset');
             
    
    });
 

  Route::group(['middleware' => 'auth'], function () {  
         /*Gandle redirection to the Dashboard*/
         Route::get('/home', ['as' => 'home', 
                                  'uses' => 'HomeController@index'
                                  ]);
           /*Handle logout*/
         Route::get('/logout', ['as'=>'logout',
                                 'uses' => 'HomeController@logout'
                                  ]);
               
                /*for messaging and chats*/
        Route::group(['prefix' => 'messages'], function () {
            Route::get('/', ['as' => 'messages', 'uses' => 'MessagesController@index']);
            Route::get('create', ['as' => 'messages.create', 'uses' => 'MessagesController@create']);
            Route::post('/', ['as' => 'messages.store', 'uses' => 'MessagesController@store']);
            Route::get('{id}', ['as' => 'messages.show', 'uses' => 'MessagesController@show']);
            Route::put('{id}', ['as' => 'messages.update', 'uses' => 'MessagesController@update']);
        });
  
     /*Blog*/
    Route::get('articles',['as' => 'articles', 'uses' => 'PostController@index']);
	Route::get('article/new',['as'=>'create-article' ,'uses'=>'PostController@create']);	 // show new post form
	Route::post('article/new','PostController@store');	// save new post	
	Route::get('article/edit/{slug}','PostController@edit');// edit post form	
	Route::post('article/update','PostController@update');	// update post
	Route::get('delete/{id}','PostController@destroy');// delete post	
	Route::get('my-all-posts','UserController@user_posts_all');	// display user's all posts
	Route::get('my-drafts','UserController@user_posts_draft');// display user's drafts	
	Route::post('comment/add','CommentController@store');// add comment	
	Route::post('comment/delete/{id}','CommentController@distroy');//delete comment


    /*Writers*/   
    Route::get('writers',['as' => 'writers', 'uses' => 'WriterController@index']);
	Route::get('writer/new',['as'=>'create-writer' ,'uses'=>'WriterController@create']);	 
	Route::post('writer/new','WriterController@store');	// s
    Route::get('writer/{code}',['as'=>'show-writer' ,'uses'=>'WriterController@show']);// 
    Route::get('writer/update/{code}',['as'=>'edit-writer' ,'uses'=>'WriterController@edit']);// 
	Route::post('writer/update','WriterController@update');	// 
	Route::get('writer/delete/{code}',['as'=>'delete-writer' ,'uses'=>'WriterController@destroy']);//
    Route::get('writer/{writer_code}/paper',['as'=>'assign-paper' ,'uses'=>'WriterController@assignPaperView']);	 
    Route::post('writer/assign-paper','WriterController@assignPaperPost');
    Route::get('writer/{id}/cv',['as'=>'download-cv' ,'uses'=>'WriterController@download']);//
    Route::post('writer/change-skill_level',['as'=>'update.skill','uses'=>'WriterController@changeSkillLevel']);
    
    Route::get('writer/submit/deletion/{code}','WriterController@submitDeletionRequest');
    Route::get('writer/approve/deletion/{code}','WriterController@approveDeletion');
 
    /*Cleints*/   
    Route::get('clients',['as' => 'clients', 'uses' => 'ClientController@index']);
	Route::get('client/new',['as'=>'create-client' ,'uses'=>'ClientController@create']);	 
	Route::post('client/new','ClientController@store');	// 	
	Route::get('client/edit/{id}',['as'=>'edit-client' ,'uses'=>'ClientController@edit']);// 
	Route::post('client/update','ClientController@update');	// 
	Route::get('client/delete/{id}',['as'=>'delete-client' ,'uses'=>'ClientController@destroy']);// 	
    Route::get('client/{id}',['as'=>'client' ,'uses'=>'ClientController@show']);// 
    
    Route::get('client/submit/deletion/{id}','ClientController@submitDeletionRequest');
    Route::get('client/approve/deletion/{id}','ClientController@approveDeletion');

     /*Papers*/   
    Route::get('papers',['as' => 'papers', 'uses' => 'PaperController@index']);
	Route::get('paper/new',['as'=>'create-paper' ,'uses'=>'PaperController@create']);	 
	Route::post('paper/new','PaperController@store');	// 	
	Route::get('paper/edit/{id}','PaperController@edit');// 
	Route::post('paper/update','PaperController@update');	// 
	Route::get('paper/delete/{id}','PaperController@destroy');// 	
    Route::get('paper/download/{id}',['as'=>'paper-download' ,'uses'=>'PaperController@download']);// 
    Route::get('paper/accept/{id}',['as'=>'paper-accept' ,'uses'=>'PaperController@accept']);//
    Route::get('paper/{id}',['as'=>'show-paper' ,'uses'=>'PaperController@show']);//  
	Route::get('paper/upload-work/{id}',['as'=>'upload-paper' ,'uses'=>'PaperController@showUpload']);//
    Route::post('paper/upload-work','PaperController@upload');//  
    Route::post('paper/assign-writer','PaperController@assignWriterPost');

    Route::get('paper/submit/deletion/{id}','PaperController@submitDeletionRequest');
    Route::get('paper/approve/deletion/{id}','PaperController@approveDeletion');

    Route::get('paper/auto/assign/{id}','PaperController@autoAccept');//
  
	//users
  	Route::get('dashboard/users',['as'=>'user-management','uses'=>'UserController@users']);// display 	
   

   //chats
   Route::get('chats','ChatController@index');


   //Admins
	Route::get('users/admin/new',['as'=>'create-admin' ,'uses'=>'AdminController@create']);	 
	Route::post('admin/new','AdminController@store');	// s	
	Route::get('admin/edit/{id}','AdminController@edit');// 
    Route::get('admin/edit/{id}',['as'=>'edit-admin' ,'uses'=>'AdminController@edit']);// 
	Route::post('admin/update','AdminController@update');	//
    Route::get('admin/delete/{id}',['as'=>'delete-admin' ,'uses'=>'AdminController@destroy']);// 




    });
 