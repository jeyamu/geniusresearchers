jQuery(document).ready(function ($) {
	"use strict";    
    
    // pages scroll to targeted section	
    $('a.page-scroll').on('click', function (event) {
        event.preventDefault();		
        $('html, body').stop().animate({
			scrollTop: $($.attr(this, 'href')).offset().top - 75
			}, 1500, 'easeInOutExpo');
        return false;
    });

    // Hide loader when window loaded
    $(window).on('load', function () {
        $(".loader-box").fadeOut("slow");
    });

    // Search
    $(".search-icon").on("click",function () {
        $(this).toggleClass("active");
        $(".search-box1").stop().fadeToggle();
        
    });

    // Display Image on popup when click on footer image icon
    $('.footer-popup-images').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });

    // Display youtube video in popup when click on about section button
    if ($('.video-play').length > 0) {
        $('.video-play').magnificPopup({
            type: 'iframe',
            removalDelay: 300,
            mainClass: 'mfp-fade'
        });
    }

    // Display Image on popup when click on portfolio image icon
    $('.portfolio_img').magnificPopup({
        type: 'image',
        gallery: {
            enabled: true
        }
    });
    
    // Slider for our team section
    var owl = $("#owl-demo");
    owl.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 2],
            [700, 3],
            [1000, 4],
            [1280, 4]
        ],
        navigation: true
    });

    // Back To Top
    $('.down-arrow').on("click", function () {
        jQuery('html, body').animate({scrollTop: 0}, 1200,'easeInOutExpo');
        return false;
    });
    var $topback = $('.down-arrow');
    var offset = 220, duration = 500;
    $(window).on('scroll', function () {
        if ($(this).scrollTop() > offset) {
            $topback.fadeIn(duration);
        } else {
            $topback.fadeOut(duration);
        }
        owo_scroll();
    });	
	jQuery(window).on('load', function(){	
		var $container = $('.portfolioContainer');
		$container.isotope({
        filter: '*',
        animationOptions: {
            duration: 750,
            easing: 'linear',
            queue: false
        }
    });
 
    $('.portfolioFilter button.button').on("click",function(){
        $('.portfolioFilter .current').removeClass('current');
        $(this).addClass('current');
 
        var selector = $(this).attr('data-filter');
        $container.isotope({
            filter: selector,
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
         });
         return false;
    }); 
	});
});

// User define function
function owo_scroll() {
    var contentTop = [];
    var contentBottom = [];
    var winTop = $(window).scrollTop();
    var rangeTop = 200;
    var rangeBottom = 500;
    $('.navbar-collapse').find('.scroll a').each(function () {
        contentTop.push($($(this).attr('href')).offset().top);
        contentBottom.push($($(this).attr('href')).offset().top + $($(this).attr('href')).height());
    });
    $.each(contentTop, function (i) {
        if (winTop > contentTop[i] - rangeTop) {
            $('.navbar-collapse li.scroll').removeClass('active').eq(i).addClass('active');
        }
    });

    var winWidth = $(window).width();
    if (winWidth < 768) {
        $(".navbar.top-menu ul li a ").on('click', function () {
            $(".navbar-toggle").trigger("click");
        });
    }
    $(window).resize(function () {
        if (winWidth < 768) {
            $(".menu ul li a").on('click', function () {
                $(".navbar-toggle").trigger("click");
            });
        }
    });

}
// Google Map
function contactAddressMap() {
    var e = new google.maps.LatLng(-33.8687999, 151.1392549),
            t = document.getElementById("googleMap"),
            a = {
                center: e,
                zoom: 10,
                scrollwheel: !1,
                styles: [
                    {
                        "featureType": "all",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            },
                            {
                                "hue": "#ff7700"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.country",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#1d1d1d"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.province",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.province",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#ed5929"
                            },
                            {
                                "weight": "5.00"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.locality",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#787878"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.locality",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "visibility": "on"
                            },
                            {
                                "weight": "5.00"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.neighborhood",
                        "elementType": "labels.text",
                        "stylers": [
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.neighborhood",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#2d2d2d"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.neighborhood",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            },
                            {
                                "visibility": "on"
                            },
                            {
                                "weight": "5.00"
                            }
                        ]
                    },
                    {
                        "featureType": "administrative.land_parcel",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "saturation": "64"
                            }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#fafafa"
                            }
                        ]
                    },
                    {
                        "featureType": "poi",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry",
                        "stylers": [
                            {
                                "color": "#2c2c2c"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#d5d5d5"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ff0000"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#ed5929"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ffffff"
                            },
                            {
                                "weight": "5.00"
                            }
                        ]
                    },
                    {
                        "featureType": "road",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#ed5929"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ed5929"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.fill",
                        "stylers": [
                            {
                                "color": "#ffffff"
                            }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "labels.text.stroke",
                        "stylers": [
                            {
                                "visibility": "on"
                            },
                            {
                                "color": "#ed5929"
                            }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry.stroke",
                        "stylers": [
                            {
                                "color": "#d9d9d9"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station",
                        "elementType": "labels.icon",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "transit.station.airport",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "visibility": "simplified"
                            },
                            {
                                "lightness": "4"
                            },
                            {
                                "saturation": "-100"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "all",
                        "stylers": [
                            {
                                "visibility": "off"
                            }
                        ]
                    },
                    {
                        "featureType": "water",
                        "elementType": "geometry.fill",
                        "stylers": [
                            {
                                "color": "#e1e1e1"
                            },
                            {
                                "visibility": "on"
                            }
                        ]
                    }
                ]
            },
    o = new google.maps.Map(t, a),
            l = new google.maps.Marker({
                position: e,
                icon: "http://preview.templety.com/owo/image/map-icon.png"
            });
    l.setMap(o)
}