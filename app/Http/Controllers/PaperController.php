<?php

namespace App\Http\Controllers;
use App\Models\Paper;
use App\Models\Client;
use Auth;
use App\Models\Writer;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Http\Requests\PaperFormRequest;
use App\Http\Requests\UploadPaperRequest;
use App\Http\Requests\AssignWriterPaperRequest;

class PaperController extends Controller
{
 private $paper;
 
 public function __construct(Paper $paper){
  $this->paper = $paper;
 }
         
  public function index() {
       $papers = "";
      	$user_id = Auth::user()->id;
		$writer = Writer::where('user_id',$user_id)->first();
        $writers = Writer::orderBy('id','desc')->get(['id','name']);
        if($writer){             
              $papers = \DB::table('papers')
            ->join('papers_for_preview', 'papers.id', '=', 'papers_for_preview.paper_id')
			->join('writers','writers.id','=','papers_for_preview.paperable_id')
            ->select('papers.*', 'papers_for_preview.paper_id','papers_for_preview.paperable_id',
            'papers_for_preview.paperable_type','papers_for_preview.submission_date',
              'papers_for_preview.status','papers_for_preview.preview_time')
			->where('papers_for_preview.paperable_id','=',$writer->id)
			->orderBy('papers_for_preview.id', 'desc')
            ->paginate(10);
        }
        else{
           $papers = $this->paper->orderBy('created_at','desc')->paginate(20);
           
        }        
        return view('dashboard.pages.paper.index',compact('papers','writers'));
    }

public function show($id) {
     $paper = $this->paper->find($id);
     $writers = \DB::table('writers')
            ->join('papers_for_preview', 'writers.id', '=', 'papers_for_preview.paperable_id')
            ->select('writers.*', 
            'papers_for_preview.paper_id','papers_for_preview.paperable_id',
             'papers_for_preview.paperable_type','papers_for_preview.submission_date',
              'papers_for_preview.status','papers_for_preview.preview_time')
			->where('papers_for_preview.paper_id','=',$id)
			->orderBy('papers_for_preview.id', 'desc')
            ->paginate(10);

     return view('dashboard.pages.paper.show',compact('paper','writers'));
    }

public function create() {
    $clients = Client::orderBy('name','desc')->get();
     return view('dashboard.pages.paper.create',compact('clients'))->with('title','GR|New Paper');
    }


  public function store(PaperFormRequest $request) {
      $paper= "";
      $author = Client::find($request->author); 
      if(!$author){
        $paper =$this->paper->create(['title'=>$request->title,
                                        'instructions'=>$request->paper_instructions,
                                        'date_received'=>$request->date_received,
                                        'due_date'=>$request->due_date,
                                        'status'=>'Unassigned',
                                        'number_of_pages'=>$request->number_of_pages]);
        }
        else{
           $paper =$author->papers()->create( ['title'=>$request->title,
                                        'instructions'=>$request->paper_instructions,
                                        'date_received'=>$request->date_received,
                                        'due_date'=>$request->due_date,
                                        'status'=>'Unassigned',
                                        'number_of_pages'=>$request->number_of_pages
                                        ]);
        }
       
       //move the file
		if(null !=($request->file('document'))){
		$path="/app/public/docs/papers/".$request->file('document')->getClientOriginalName();
       
        $request->file('document')->move(storage_path('/app/public/docs/papers/')
		                         ,$request->file('document')->getClientOriginalName());
		
		//create a doc
		$paper->files()->create(['description'=>$request->title,
		                           'path'=>$path,'is_verified'=>0,
                                    'status'=>'Unassigned']);
		} 
    

        if ($paper) {
            return redirect()->route('papers')->with('message','Paper Added succesfully');
        } else {
            return redirect()->back()->with('error_message','Paper registration failed');
        }
    }


 public function download($id) {
    $paper = $this->paper->find($id);

    $paper=\DB::table('documents')->where('fileable_id',$paper->id)->first();
     try{
       $path = storage_path($paper->path);
       return response()->download($path);
     }catch(FileNotFoundException $ex){
         echo "error";
         return redirect()->back();
     }
    }

   public function accept($id) {
    /*Picked paper id being updated, 
     the pick the logged in writer too
    */
   $paper = $this->paper->find($id);

    $user_id = Auth::user()->id;
	$writer = Writer::where('user_id',$user_id)->first();    
   /*Update the status of the Paper
   ,we need to know the time the update took place
   */
   $updated= \DB::table('papers_for_preview')
            //->where('paperable_id', '=',$writer->id)
            ->where('paper_id', '=',$id)
            ->update(['status' => 'Accepted','updated_at'=>Carbon::now()]);
    
     $papers= \DB::table('papers')
            //->where('paperable_id', '=',$writer->id)
            ->where('id', '=',$id)
            ->update(['status' => 'Accepted','updated_at'=>Carbon::now()]);
    
    
       return redirect()->back()->with('message','Your acceptance of writing the paper '.$paper->title. 'has been acknowlegded');
    }

    public function showUpload($id) {
     $paper = $this->paper->find($id);
     return view('dashboard.pages.paper.upload',compact('paper'));
    }

/*Here the writer uploads the work he has done*/
public function upload(UploadPaperRequest $request) {
     
      $paper = $this->paper->find($request->paper_id);
       //update paper status
       $updated= \DB::table('papers_for_preview')
            //->where('paperable_id', '=',$writer->id)
            ->where('paper_id', '=',$paper->id)
            ->update(['status' => 'Pending Approval','updated_at'=>Carbon::now()]);
    
      $papers= \DB::table('papers')
            ->where('id', '=',$paper->id)
            ->update(['status' => 'Pending Approval','updated_at'=>Carbon::now()]);
        
       //move the file
		if(null !=($request->file('document'))){
		$path="/app/public/docs/papers/".$request->file('document')->getClientOriginalName();
       
        $request->file('document')->move(storage_path('/app/public/docs/papers/')
		                         ,$request->file('document')->getClientOriginalName());
		
		//update doc details after upload
         $doc= \DB::table('documents')
            ->where('fileable_id', '=',$paper->id)
            ->update(['path' => $path,'is_verified'=>1,'updated_at'=>Carbon::now()]);
		} 
    

        if ($doc) {
            return redirect()->route('papers')->with('message','Paper Uploaded succesfully');
        } else {
            return redirect()->back()->with('error_message','Paper regsitration failed');
        }
    }

//Assighn the paper to writer
public function assignWriterPost(AssignWriterPaperRequest $request) {
    //pick writer details
    $writer = Writer::find($request->writer);
     
     //check if this paper has already been assigned to this writer.
     $paper=\DB::table('papers_for_preview')->where('paper_id',$request->paper_id)
                                              ->where('paperable_id',$writer->id)
                                              ->first();    
    if($paper){
        return redirect()->back()->with('error_message','This paper has already been assigned to this writer!');
    }

    //update the Preview table
     $paper_preview = \DB::table('papers_for_preview')->insert(['paper_id'=>$request->paper_id,
                                               'paperable_id'=>$writer->id,
                                               'paperable_type'=>'App\Models\Writer',
                                               'status'=>'Assigned',
                                               'submission_date'=>$request->due_date]);

    
    //update the paper table
     $paper_update= \DB::table('papers')
            ->where('id', '=',$request->paper_id)
            ->update(['status' => 'Assigned','updated_at'=>Carbon::now()]);
     
       $message = 'Writer '.$writer->name. " assigned a paper successfully, 
                            the work acceptance should be confirmed by writer in 10 minutes";

     return redirect()->route('writers')->with('message',$message);
    }


    public function submitDeletionRequest($id)
  {
      $paper =$this->paper->find($id);
      $paper->for_deletion =1; //Request For Deletion
      $paper->save();
      return redirect()->back()->with('message','Your request to delete this paper has been submited,
                                  Wait for an approval from the Main Admin so as to delete it. Thank you');
  }


  public function approveDeletion($id)
  {
      $paper =$this->paper->find($id);
      $paper->for_deletion =2; //Approve it for deletion, the Admin can then delete this paper
      $paper->save();
     return redirect()->back()->with('message','You have approved to delete this Paper from the system. Thank you');
  }



  //auto accept the paper
public function autoAccept($id) {
    $paper = $this->paper->find($id);

    $user_id = Auth::user()->id;
	$writer = Writer::where('user_id',$user_id)->first();    
   /*Update the status of the Paper
   ,we need to know the time the update took place
   */
   $updated= \DB::table('papers_for_preview')
            //->where('paperable_id', '=',$writer->id)
            ->where('paper_id', '=',$id)
            ->update(['status' => 'Accepted','updated_at'=>Carbon::now()]);
    
     $papers= \DB::table('papers')
            //->where('paperable_id', '=',$writer->id)
            ->where('id', '=',$id)
            ->update(['status' => 'Accepted','updated_at'=>Carbon::now()]);
    
    
       return redirect()->back()->with('message','You have automatically accepted writing the paper '.$paper->title);
    }




}
