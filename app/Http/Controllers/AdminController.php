<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\User;
use App\Http\Requests\AdminFormRequest;
use App\Http\Requests\UpdateAdminFormRequest;

class AdminController extends Controller
{

 private $admin;
 
 public function __construct(Admin $admin){
     $this->admin = $admin;
 }
         
 public function create() {
     return view('dashboard.pages.admin.create')->with('title','GR|New Admin');
 }


  public function store(AdminFormRequest $request) {
      $user = User::create(['username'=>$request->email,
                            'password'=>bcrypt($request->password),
                            'role' =>'Admin',
                            'email'=>$request->email]);

      $admin =$this->admin->create( ['user_id'=>$user->id,
                                        'name'=>$request->firstname ." ".$request->lastname,
                                        'phone'=>$request->phone,
                                        'gender'=>$request->gender
                                        ]);  
         //move the users photo
		if(null !=($request->file('photo'))){
		$path="app/admins/photos/".$request->file('photo')->getClientOriginalName();
         
        $request->file('photo')->move(public_path('app/admins/photos/')
		                         ,$request->file('photo')->getClientOriginalName());
		
		//create a photo
		$admin->photos()->create(['description'=>$request->firstname ."_".$request->lastname,
		                           'path'=>$path]);
		}       

        if ($admin) { 
            return redirect()->route('user-management')->with('message','Admin Added Succesfully');
        } else {
            return redirect()->route('user-management')->with('message', 'There is a problem, saving the Admin Details in the Database');
        }
    }


    public function edit($id) {
     $admin = $this->admin->find($id);      
		  
     return view('dashboard.pages.admin.edit',compact('admin'));
    }

public function update(UpdateAdminFormRequest $request){

    $admin = $this->admin->find($request->id);

    //update admin table
    $admin->name = $request->name;
    $admin->phone = $request->phone;
    $admin->gender = $request->gender;
    $admin->save();

    $message = 'Administrator' . $request->name . " details updated succesfully";

   return redirect()->route('user-management')->with('message',$message);
}

public function destroy($id){
    $admin = $this->admin->find($id); 
    //first Admin records from user table
    $user = User::find($admin->user_id); 
    $user->delete();

    $deleted_admin =$admin;

    //first Admin records
    $admin->delete();  

   $message = 'Administrator '. $deleted_admin->name . " succesfully deleted";

   return redirect()->route('user-management')->with('message',$message);
}


}
