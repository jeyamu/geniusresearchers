<?php namespace App\Http\Controllers;
Use App\Models\Posts;


class WelcomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Welcome Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the "marketing page" for the application and
	| is configured to only allow guests. Like most of the other sample
	| controllers, you are free to modify or remove it as you desire.
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('guest');
	}

	/**
	 * Show the application welcome screen to the user.
	 *
	 * @return Response
	 */
	  //
    public function index(){
		 $posts = Posts::where('active','1')->orderBy('created_at','desc')->paginate(5);
        return view('site.pages.home',compact('posts'))->with('title','GeniusResearchers|Home');
    }

	public function contact(){
        return view('site.pages.contact')->with('title','GeniusResearchers|Contact Us');
    }

	public function blog($slug){

		$post = Posts::where('slug',$slug)->first();

		if($post)
		{
			if($post->active == false)
				return redirect('/')->withErrors('requested page not found');
			$comments = $post->comments;	
		}
		else 
		{
			return redirect('/')->withErrors('requested page not found');
		}
		$title = 'GeniusResearchers|Blog';
        return view('site.pages.blog',compact('post','title'));
    }

	public function about(){
        return view('site.pages.about')->with('title','GeniusResearchers|About Us');
    }

	public function login(){
        return view('site.pages.login')->with('title','GeniusResearchers|Login');
    }

}