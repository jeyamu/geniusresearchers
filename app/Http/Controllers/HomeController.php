<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Writer;

class HomeController extends Controller
{
   public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $papers = null;
		 
		$user_id = Auth::user()->id;
		$writer = Writer::where('user_id',$user_id)->first();
		$user_type= Auth::user()->role;
				

		if($writer) {
		  $papers = \DB::table('papers')
            ->join('papers_for_preview', 'papers.id', '=', 'papers_for_preview.paper_id')
			->join('writers','writers.id','=','papers_for_preview.paperable_id')
            ->select('papers.*', 'papers_for_preview.*','writers.*')
			->where('papers_for_preview.paperable_id','=',$writer->id)
			->orderBy('papers_for_preview.id', 'desc')
            ->paginate(10);
		}
		else{

		    $papers = \DB::table('papers')
            ->join('papers_for_preview', 'papers.id', '=', 'papers_for_preview.paper_id')
			->join('writers','writers.id','=','papers_for_preview.paperable_id')
            ->select('papers.*', 'papers_for_preview.*','writers.*')
			->orderBy('papers_for_preview.id', 'desc')
			->paginate(10);

		}
         //users
		 $users = \DB::table('users')
            ->join('writers', 'users.id', '=', 'writers.user_id')
            ->select('users.*', 'writers.*')
			->orderBy('users.id', 'desc')
            ->paginate(10);
		
		 //clients
		 $clients = \DB::table('clients')
			->orderBy('id', 'desc')
            ->paginate(10);
		
		return view('dashboard.pages.dashboard',compact('papers','user_type','clients'))->with('title','GR|Dashboard');
	}

	
     /*logout the User*/
    public function logout(){
        Auth::logout();
       return redirect()->route('login');
    }


}
