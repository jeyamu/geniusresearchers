<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Session;
use App\Models\User;
use App\Models\Posts;
use App\Models\Admin;
use App\Models\Client;
use App\Models\Writer;
use Auth;

class UserController extends Controller {

      /* login */

  public function authenticate(LoginRequest $request) {
      $loggedIn =false;
	  /*if user has checked the remember me check box*/

	
	  	  if(isset($request->remember))
	  {
		  $loggedIn =Auth::attempt(['username' => $request->username, 
		                 'password' => $request->password],$request->remember);
	  }
	  else{
		  $loggedIn =Auth::attempt(['username' => $request->username, 
		                           'password' => $request->password]
						 ); 
	  }

	  if($loggedIn){
        //setting session
            Session::put('username', $request->username);

            return redirect()->intended('/home')->with('title', 'GR|Home');
	  } else {
            
            return redirect()->route('login')->with('title', 'GR|Sign In')
			                                 ->with('error_message','Wrong Username or Password');
        }
    }
    
  
  public function users(){
	  $admins =Admin::orderBy('id','desc')->paginate(20);
	  $writers=Writer::orderBy('id','desc')->paginate(20);
	  $clients =Client::orderBy('id','desc')->paginate(20);
  
	  return view('dashboard.pages.users',compact('admins','writers','clients'));
  }


}
