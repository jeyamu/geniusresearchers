<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\Writer;
Use App\Models\Paper;
Use App\Models\User;
use App\Http\Requests\WriterFormRequest;
use App\Http\Requests\UpdateWriterFormRequest;
use App\Http\Requests\UpdateSkillLevelRequest;

use App\Http\Requests\AssignPaperFormRequest;
use Auth;
use Carbon\Carbon;

class WriterController extends Controller
{
 private $writer;
 
 public function __construct(Writer $writer){
  $this->writer = $writer;
 }
         
  public function index() {
        $writers = $this->writer->orderBy('created_at','desc')->paginate(20);
        $papers =  Paper::orderBy('created_at','desc')->get();
        
        return view('dashboard.pages.writer.index',compact('writers','papers'));
    }

public function show($code) {
     $writer = $this->writer->where('writer_code','=',$code)->first(); 
     $papers = \DB::table('papers')
            ->join('papers_for_preview', 'papers.id', '=', 'papers_for_preview.paper_id')
            ->select('papers.*', 'papers_for_preview.*')
			->where('papers_for_preview.paperable_id','=',$writer->id)
            ->get();
     //Get Writers photos
    $photo = \DB::table('photos')
			->where('imageable_id','=',$writer->id)
            ->where('imageable_type','=','App\Models\Writer')
            ->first();
		  
     return view('dashboard.pages.writer.show',compact('writer','papers','cv','photo'));
    }

public function edit($code) {
     $writer = $this->writer->where('writer_code','=',$code)->first();      
		  
     return view('dashboard.pages.writer.edit',compact('writer'));
    }

public function update(UpdateWriterFormRequest $request){
    $writer = $this->writer->find(request()->id); 

   $writer->name = request()->name;
   $writer->nin = request()->nin;
   $writer->place_of_residence = request()->place_of_residence;
   $writer->skill_level = request()->skill_level;
   $writer->skill_level = request()->skill_level;
   $writer->date_of_birth = request()->date_of_birth;
   $writer->qualification = request()->qualification;
   $writer->skill_level = request()->skill_level;
   $writer->gender = request()->gender;
   $writer->nationality = request()->nationality;
   $writer->save();

   $message = 'Writer' . request()->name . " details updated succesfully";

   return redirect()->route('user-management')->with('message',$message);
}

public function destroy($code){
    $writer = $this->writer->where('writer_code','=',$code)->first(); 
    $deleted_writer =$writer;

     //first Writer records from user table
    $user = User::find($writer->user_id); 
    $user->delete();
    
    //Delete writer Records
    $writer->delete();  

   $message = 'Writer '. $deleted_writer->name . " succesfully deleted";

   return redirect()->route('user-management')->with('message',$message);
}




public function create() {
     return view('dashboard.pages.writer.create')->with('title','GR|New Writer');
    }


  public function store(WriterFormRequest $request) {

      $user = User::create(['username'=>$request->email,
                            'password'=>bcrypt($request->password),
                            'role' =>'Writer',
                            'email'=>$request->email]);

      $writer =$this->writer->create( ['user_id'=>$user->id,
                                        'name'=>$request->firstname ." ".$request->lastname,
                                        'nin'=>$request->nin,
                                        'writer_code'=>$this->genWriterCode(),
                                        'place_of_residence'=>$request->place_of_residence,
                                        'date_of_birth'=>$request->date_of_birth,
                                        'qualification'=>$request->qualification,
                                        'nationality'=>$request->nationality,
                                        'gender'=>$request->gender,
                                        'skill_level'=>$request->skill_level
                                        ]);
        
       //move the users photo
		if(null !=($request->file('photo'))){
		$path="app/writers/photos/".$request->file('photo')->getClientOriginalName();
         
        $request->file('photo')->move(public_path('app/writers/photos/')
		                         ,$request->file('photo')->getClientOriginalName());
		
		//create a photo
		$writer->photos()->create(['description'=>$request->firstname ."_".$request->lastname,
		                           'path'=>$path]);
		} 
        

        if(null !=($request->file('cv'))){
		$path="/app/public/users/docs/".$request->file('cv')->getClientOriginalName();

        $request->file('cv')->move(storage_path('/app/public/users/docs/')
		                         ,$request->file('cv')->getClientOriginalName());
		//create a cv
		$writer->files()->create(['description'=>$request->firstname ."_".$request->lastname,
		                           'path'=>$path,'is_verified'=>0]);
		} 


        if ($writer) {
            $message = "Dear, ".$writer->name ." you have been succesfully registered, your Identity code is ". $writer->writer_code;           
           
            return redirect()->route('writers')->with('message',$message);
        } else {
            return redirect()->back()->with('error_message', 'Writer registration failed');
        }
    }

/*Generate a random code for a writer*/
 private function genWriterCode(){
    $code = "GW".substr(md5(rand()), 0, 5);
    return $code;
  }


public function assignPaperView($writer_code) {
      $writer = $this->writer->where('writer_code','=',$writer_code)->first();
      $papers =    Paper::orderBy('id','desc')->get();
     return view('dashboard.pages.writer.assign_paper',compact('writer','papers'))->with('title','GR|Assign Writer a Paper');
    }


public function assignPaperPost(AssignPaperFormRequest $request) {
    //pick writer details
    $writer = $this->writer->find($request->writer_id);
    $paper=\DB::table('papers_for_preview')->where('paper_id',$request->paper)
                                              ->where('paperable_id',$writer->id)
                                              ->first();
    
    if($paper){
        return redirect()->back()->with('error_message','This paper has already been assigned to this writer!');
    }
    
    //update the Preview table
     $paper_preview = \DB::table('papers_for_preview')->insert(['paper_id'=>$request->paper,
                                               'paperable_id'=>$writer->id,
                                               'paperable_type'=>'App\Models\Writer',
                                               'status'=>'Assigned',
                                               'submission_date'=>$request->due_date]);

    
    //update the paper table
     $paper_update= \DB::table('papers')
            ->where('id', '=',$request->paper)
            ->update(['status' => 'Assigned','updated_at'=>Carbon::now()]);
     
       $message = 'Writer '.$writer->name. " assigned a paper successfully, 
                            the work acceptance should be confirmed by writer in 10 minutes";

     return redirect()->route('writers')->with('message',$message);
    }



 public function download($id) {
      //Get Writers Cv
    $cv = \DB::table('documents')
			->where('fileable_id','=',$id)
            ->where('fileable_type','=','App\Models\Writer')
            ->first();

     try{
       $path = storage_path($cv->path);
       return response()->download($path);
     }catch(FileNotFoundException $ex){
         return redirect()->back();
     }
    }

    public function changeSkillLevel(UpdateSkillLevelRequest $request)
    {
    $writer = $this->writer->find($request->writer_id); 

    $writer->skill_level = $request->skill_level;
    $writer->save();

   $message = 'Writer '. $writer->name." 's  skill level updated to ".$request->skill_level . " succesfully";

   return redirect()->route('writers')->with('message',$message);
   }


    public function submitDeletionRequest($id)
  {
      $paper =$this->writer->find($id);
      $paper->for_deletion =1; //Request For Deletion
      $paper->save();
      return redirect()->back()->with('message','Your request to delete this Writer has been submited,
                                  Wait for an approval from the Main Admin so as to delete it. Thank you');
  }

  public function approveDeletion($id)
  {
      $paper =$this->writer->find($id);
      $paper->for_deletion =2; //Approve it for deletion, the Admin can then delete this writer
      $paper->save();
     return redirect()->back()->with('message','You have approved to delete this Writer from the system. Thank you');
  }



}
