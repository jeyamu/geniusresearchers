<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use App\Models\Client;

use App\Http\Requests\ClientFormRequest;
use App\Http\Requests\UpdateClientFormRequest;

class ClientController extends Controller
{
 private $client;
 
 public function __construct(Client $client){
  $this->client = $client;
 }
         
  public function index() {
        $clients = $this->client->orderBy('created_at','desc')->paginate(20);
        return view('dashboard.pages.client.index',compact('clients'));
    }

public function show($id) {

      $client = $this->client->find($id);
      //get papers authored by the client
      $papers = \DB::table('papers')
			->where('client_id','=',$client->id)
            ->get();
    //Get Cleints Photo
    $photo = \DB::table('photos')
			->where('imageable_id','=',$client->id)
            ->where('imageable_type','=','App\Models\Client')
            ->first();
		  
     return view('dashboard.pages.client.show',compact('client','papers','photo'));
    }

public function create() {
     return view('dashboard.pages.client.create')->with('title','GR|New Client');
    }


  public function store(ClientFormRequest $request) {

      $client =$this->client->create( ['name'=>$request->firstname ." ".$request->lastname,
                                        'location'=>$request->location,
                                        'date_of_joining'=>$request->date_of_joining,
                                        'phone'=>$request->phone,
                                        'email'=>$request->email
                                        ]);
        
       //move the Cleint photo
      try{
		if(null !=($request->file('photo'))){
		$path="app/clients/photos/".$request->file('photo')->getClientOriginalName();
        
        $request->file('photo')->move(public_path('/app/clients/photos/')
		                         ,$request->file('photo')->getClientOriginalName());
		
		//create a photo
		$client->photos()->create(['description'=>$request->firstname ."_".$request->lastname,
		                           'path'=>$path]);
		} 
      }catch(Exception $ex){

          return redirect()->back()->with('error_message','Client saved but, Photo has not been moved');
      }
    
     return redirect()->route('clients')->with('message','Client information saved succefully');
    
    }

public function edit($id) {
     $client = $this->client->find($id);      
		  
     return view('dashboard.pages.client.edit',compact('client'));
    }

public function update(UpdateClientFormRequest $request){
    $client = $this->client->find(request()->id); 

    $client->name = $request->name;
    $client->phone = $request->phone;
    $client->email = $request->email;
    $client->location = $request->location;
    $client->date_of_joining = $request->date_of_joining;
    $client->save();

    $message = 'Client' . $request->name . " details updated succesfully";

   return redirect()->route('user-management')->with('message',$message);
}

public function destroy($id){
    $client = $this->client->find($id); 
    $deleted_client =$client;
    $client->delete();  

   $message = 'Client '. $deleted_client->name . " succesfully deleted";

   return redirect()->route('user-management')->with('message',$message);
}


    public function submitDeletionRequest($id)
  {
      $paper =$this->client->find($id);
      $paper->for_deletion =1; //Request For Deletion
      $paper->save();
      return redirect()->back()->with('message','Your request to delete this Client has been submited,
                                  Wait for an approval from the Main Admin so as to delete it. Thank you');
  }


  public function approveDeletion($id)
  {
      $paper =$this->client->find($id);
      $paper->for_deletion =2; //Approve it for deletion, the Admin can then delete this paper
      $paper->save();
      return redirect()->back()->with('message','You have approved to delete this Client from the system. Thank you');
  }

}
