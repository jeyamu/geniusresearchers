<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WriterFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed|min:3',
            'password_confirmation' => 'required|min:3',
			'nin' => array('Regex:/^[A-Za-z0-9 ]+$/','unique:writers'),
			'place_of_residence' => 'required',
            'date_of_birth' => 'required|date',
            'qualification' => 'required',
            'nationality' => 'required',
            'skill_level' => 'required',
            'gender' => 'required',
            'cv' => 'required|file',
            'photo' => 'image',
        ];
    }
}
