<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaperFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|unique:papers',
            'comments' => ['Regex:/^[A-Za-z]+$/'],
            'paper_instructions' => 'required',
            'date_received' => 'required|date',
            'due_date' => 'required|date',
            'document' => 'required|file',
            'number_of_pages' => 'required',
            'number_of_pages' => ['Regex:/^[0-9]+$/'],
            'author' => ''
        ];
    }
}
