<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateWriterFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
			'nin' => array('Regex:/^[A-Za-z0-9 ]+$/','required'),
			'place_of_residence' => 'required',
            'date_of_birth' => 'required|date',
            'qualification' => 'required',
            'nationality' => 'required',
            'skill_level' => 'required',
            'gender' => 'required'
        ];
    }
}
