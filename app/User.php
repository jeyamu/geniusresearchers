<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Cmgmyr\Messenger\Traits\Messagable;
use Illuminate\Notifications\Notifiable; 

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	 use Authenticatable, CanResetPassword,Notifiable;
	 use Messagable;

	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['username','email','name','role','password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];


	
   /*Writer is a User*/
    public function writer()
    {
       return $this->belongsTo('App\Models\Writer');
    }

	  /*Admin is a User*/
    public function admin()
    {
       return $this->belongsTo('App\Models\Admin');
    }


	 /*USer has files, profile photo*/
   public function files()
    {
        return $this->morphMany('App\Models\Document', 'fileable');
    }
    
    

}
