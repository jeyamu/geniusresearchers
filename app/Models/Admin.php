<?php  namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Admin extends Model  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'admins';
    protected $fillable=['name','phone','gender','user_id'];
    
  
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */


     public function user()
    {
        return $this->hasOne('App\Models\User');
    }

	/*Admin assigns paper to writer*/
    public function writers()
    {
         return $this->hasMany('App\Models\Writer');
    }


	 
  /*Admin Has Docs*/
   public function files()
    {
        return $this->morphMany('App\Models\Document', 'fileable');
    }

    	 
  /*Admin Has Photos*/
   public function photos()
    {
        return $this->morphMany('App\Models\Photo', 'imageable');
    }

 /*Admin can review a paper, that can in turn be reviewed*/

  public function reviews()
    {
        return $this->morphToMany('App\Models\Review', 'reviewable');
    }

	
	

}
