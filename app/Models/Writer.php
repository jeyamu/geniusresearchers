<?php  namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Writer extends Model  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'writers';
    protected $fillable=['user_id','writer_code','name','for_deletion','nationality','place_of_residence','nin',
	                    'date_of_joining','skill_level','date_of_birth','qualification','gender'];
    
  
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */


     public function user()
    {
        return $this->hasOne('App\Models\User');
    }

	  /*Admin assigns paper to writer*/
	public function admin()
    {
       return $this->belongsTo('App\Models\Admin');
    }

	   /*Papers can be assigned to many writers 
    public function papers()
    {
         return $this->belongsToMany('App\Models\Paper')
                    ->withPivot('status')
                    ->withTimestamps();
    }
*/

	 
  /*Writer has Docs*/
   public function files()
    {
        return $this->morphMany('App\Models\Document', 'fileable');
    }

        	 
  /*Writer Has Photos*/
   public function photos()
    {
        return $this->morphMany('App\Models\Photo', 'imageable');
    }

 /*Writer can be assigned to many papers*/

  public function papers()
    {
        return $this->morphToMany('App\Models\Paper', 'paperable');
    }
	
	

}
