<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model {
    protected  $table='clients';
    protected $fillable=['name','phone','email','location','date_of_joining','for_deletion'];
     
   /*Client has Many Papers */
    public function papers()
    {
        return $this->hasMany('App\Models\Paper');
    }
 

  /*Cleint might have a photo*/
   public function photos()
    {
        return $this->morphMany('App\Models\Photo', 'imageable');
    }
 

}
