<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model {

	//posts table in database
	protected $guarded = [];
	public function comments()
	{
		return $this->hasMany('App\Models\Comments','on_post');
	}
	
	public function author()
	{
		return $this->belongsTo('App\Models\User','author_id');
	}

	  /*Post/Article Has Photos*/
    public function photos()
    {
        return $this->morphMany('App\Models\Photo', 'imageable');
    }


}