<?php namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Paper extends Model  {


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'papers';
    protected $fillable = ['title','date_received','due_date','status',
                        'instructions','comments','number_of_pages','for_deletion'];
	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

	  /*Client has Many Papers */
    public function client()
    {
        return $this->belongsTo('App\Models\Client');
    }
 
 /**
     * Get all of the admins that are assigned this paper.
     */
    public function admins()
    {
        return $this->morphedByMany('App\Models\Admin', 'paperable');
    }

    /**
     * Get all of the writers that are assigned this paper.
     */
    public function writers()
    {
        return $this->morphedByMany('App\Models\Writer', 'paperable');
    }

    	 
  /*Paper belongs a path*/
   public function files()
    {
        return $this->morphMany('App\Models\Document', 'fileable');
    }


}
