<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model {

	//
    protected  $table="documents";
    protected  $fillable= ['name','description','path','is_verified','verified_by','fileable_id',
                           'fileable_type'];
 
    public function fileable()
    {
        return $this->morphTo();
    }
    
}
