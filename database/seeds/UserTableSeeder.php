<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Admin;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*Create super admin when installing the app
        */

        $user =User::create([
            'username' => 'super',
            'email' => 'super@gmail.com',
            'role' => 'Super Admin',
            'password' => bcrypt('secret'),
        ]);

        $admin =Admin::create([
            'user_id'=>$user->id,
            'name' => 'Super Admins',
            'phone' => '0784197545',
            'gender' => 'Male']);
    }
}
