<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppTable extends Migration {

	/*1. keep track of staticsts, n=total number of papers accepted
       ,accepted,rejected,

	   2. papper_writer may need to become a polymorphic table,

	   its being used by the writter and the Admins for preview

	   3. We need photo of writers and admins
	   4. Cv of writers

	   5. Identifiers for writers, random generated ID eg GRC10101

	   6. Before a writer moves to the next assigment, they should have finisehed the first assigmnet
       7. Create a table for rules, say if writer is junior theare are a number of paper they are needed to write
       8.  Password recovery
	   9. Online chat among writers and admins
	   10. Password change, user management
	   11. shared work

	   12. writer needs to upload paper after he has finished work
	*/

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('username')->unique();
			$table->string('password', 60);
			$table->string('role')->nullable();
			$table->string('email')->unique();
			$table->integer('for_deletion')->default(0); //0 deltion not triggered, 1 deletion triggered,2,deletion approved
			$table->rememberToken();
			$table->timestamps();
		});
       
	 	Schema::create('password_resets', function(Blueprint $table)
		{
			$table->string('email')->index();
			$table->string('token')->index();
			$table->timestamp('created_at');
		});
	

        
		/*Admin*/
		Schema::create('admins', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->nullable();
			$table->string('name');
			$table->string('phone')->nullable();
			$table->enum('gender',['Male','Female']);
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
		});
        
		/*Writers*/
		Schema::create('writers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->nullable();
			$table->integer('admin_id')->unsigned()->nullable();
			$table->string('writer_code')->nullable();
			$table->string('name');
			$table->string('nationality')->nullable();
			$table->string('nin')->nullable();//national id number
			$table->string('place_of_residence')->nullable();
			$table->enum('skill_level',['Freshman','Junior','Intermediate','Expert']);
			$table->string('date_of_birth');
			$table->string('qualification');
			$table->enum('gender',['Male','Female']);
			$table->integer('for_deletion')->default(0); //0 deltion not triggered, 1 deletion triggered,2,deletion approved
			$table->timestamps();
			$table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('admin_id')->references('id')->on('admins')->onDelete('cascade')->onUpdate('cascade');
		});


		/*Clients*/
		Schema::create('clients', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('phone');
			$table->string('email')->nullable();
			$table->string('location');
			$table->string('date_of_joining')->nullable();
			$table->integer('for_deletion')->default(0); //0 deltion not triggered, 1 deletion triggered,2,deletion approved
			$table->timestamps();
		});
        

		/*Papers*/
		Schema::create('papers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_id')->unsigned()->nullable();
			$table->string('title');
			$table->date('date_received');
			$table->date('due_date')->nullable(); //date client needs the paper done
			$table->string('status')->nullable();
			$table->text('instructions')->nullable();
			$table->integer('number_of_pages')->nullable();
			$table->string('comments')->nullable();
			$table->integer('for_deletion')->default(0); //0 deltion not triggered, 1 deletion triggered,2,deletion approved
			$table->timestamps();
			$table->foreign('client_id')->references('id')->on('clients')->onDelete('cascade')->onUpdate('cascade');
		});
        
        
		  
		/*Papers^ Writer(admins*/
		Schema::create('papers_for_preview', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('paper_id')->unsigned()->nullable(); //id of paper
			$table->integer('paperable_id')->unsigned()->nullable(); //id of either Writer or Admin
			$table->string('paperable_type')->nullable();
			$table->string('submission_date')->nullable(); //writer submits to the admin
			$table->string('status');
			$table->double('preview_time')->default(0);//used by a writer to accept or reject work in minutes			
			$table->timestamps();
		
		});
			/*Photos*/
		Schema::create('photos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('imageable_id')->unsigned()->nullable(); //id of either Writer or Admin
			$table->string('imageable_type')->nullable();
			$table->string('description')->nullable(); //writer submits to the admin
			$table->string('path')->nullable();
			$table->timestamps();		
		});

				/*Documets*/
		Schema::create('documents', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('fileable_id')->unsigned()->nullable(); 
			$table->string('fileable_type')->nullable();
			$table->string('description')->nullable(); 
			$table->string('path')->nullable();
			$table->string('name')->nullable();
			$table->string('is_verified')->default('No');
			$table->string('verified_by')->nullable();
			$table->string('status')->nullable();//track papers status
			$table->timestamps();
		
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
		Schema::drop('admins');
		Schema::drop('writers');
		Schema::drop('papers');
		Schema::drop('papers_for_preview');
	}

}
