   <footer class="footer">
            <div class="container-fluid">
                <nav class="pull-left">
                    <ul>
                        <li>
                            <a href="{{route('home')}}">
                                Go to Website
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Papers
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                Writers
                            </a>
                        </li>
                        <li>
                            <a href="#">
                               Clients
                            </a>
                        </li>
                    </ul>
                </nav>
                <p class="copyright pull-right">
                    &copy; <?php echo Date('Y');?> Genius Writers</a>, By Joel Eyamu <small>(joeleyamu@gmail.com)</small> & Jurugu Brian</p>
            </div>
        </footer>

    </div>
</div>

    <!--   Core JS Files   -->
     <script src="{{asset('js/jquery-1.11.3.min.js')}}" type="text/javascript"></script>
    <!--<script src="{{asset('dashboard/js/jquery-1.10.2.js')}}" type="text/javascript"></script>-->
    <script src="{{asset('dashboard/js/bootstrap.min.js')}}" type="text/javascript"></script>
   
    <script type="text/javascript" src="{{asset('dashboard/js/bootstrap-multiselect.js')}}"></script>
	<!--  Checkbox, Radio & Switch Plugins -->
     <script src="{{asset('dashboard/js/bootstrap-checkbox-radio-switch.js')}}" type="text/javascript"></script>