<div class="wrapper">  
 <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">
   <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="" class="simple-text">
                    Genius Writers
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="{{route('home')}}">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                @if(Auth::user()->role == 'Admin' || Auth::user()->role == 'Super Admin')
                <li>
                    <a href="{{route('user-management')}}">
                        <i class="pe-7s-user"></i>
                        <p>User Management</p>
                    </a>
                </li>
                @endif
                <li>
                    <a href="{{route('writers')}}">
                        <i class="pe-7s-note2"></i>
                        <p>Writer Management</p>
                    </a>
                </li>
                <li>
                    <a href="{{route('papers')}}">
                        <i class="pe-7s-news-paper"></i>
                        <p>Paper Management</p>
                    </a>
                </li>
               @if(Auth::user()->role == 'Admin' || Auth::user()->role == 'Super Admin')
                <li>
                    <a href="{{route('clients')}}">
                        <i class="pe-7s-science"></i>
                        <p>Client Management</p>
                    </a>
                </li>
                @endif
				
            </ul>
    	</div>
    </div>