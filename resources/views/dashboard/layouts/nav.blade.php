   <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{route('home')}}">Dashboard</a>
                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                     <li>
                           <a href="">
                              
                            </a>
                        </li>
                        <li>
                           <a href="">
                               Messages
                            </a>
                        </li>
                        <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    @include('dashboard.messenger.unread-count')
                                    <b class="caret"></b>
                              </a>
                              <ul class="dropdown-menu">
                                 <li><a href="/messages">Messages @include('dashboard.messenger.unread-count')</a></li>
                                <li><a href="/messages/create">Compose Message</a></li>
                              </ul>
                        </li>
                        <li>
                        <?php    
                        if(Auth::user()->role =='Writer') {
                          $writer = App\Models\Writer::where('user_id',Auth::user()->id)->first(['skill_level']);
                          $skill_level = $writer->skill_level;
                        }   
                        else{
                            $skill_level = Auth::user()->role;
                        }                
                           
                        ?>
                           @if(Auth::check())                         
                            <a href="{{route('logout')}}">
                            <span class="label label-info">{{$skill_level}}</span>
                               {{Auth::user()->username}} (Logout)
                            </a>
                               
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
