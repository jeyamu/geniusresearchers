	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>@if(isset($title)){{$title}}@endif</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
      <link rel="stylesheet" href="{{asset('dashboard/css/bootstrap.min.css')}}" type="text/css">
  
    <!--  Light Bootstrap Table core CSS    -->
    <link rel="stylesheet" href="{{asset('dashboard/css/light-bootstrap-dashboard.css')}}" type="text/css">

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="{{asset('dashboard/css/pe-icon-7-stroke.css')}}" type="text/css">
   
        <!-- Latest compiled and minified CSS -->
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">
          <!-- Date Range Picker     -->
      <link rel="stylesheet" href="{{asset('css/daterangepicker.css')}}" type="text/css">
      <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
