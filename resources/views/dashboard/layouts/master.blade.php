<!doctype html>
<html lang="en">
<head>
@include('dashboard.layouts.header')      
</head>
<body>
@include('dashboard.layouts.sidebar')
@include('dashboard.layouts.nav')
@yield('content')
</body>
@include('dashboard.layouts.footer')
@yield('javascript')
</html>

