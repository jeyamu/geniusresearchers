
    <!--   Core JS Files   -->
     <script src="{{asset('js/jquery-1.11.3.min.js')}}"></script>
    <!--<script src="{{asset('dashboard/js/jquery-1.10.2.js')}}" type="text/javascript"></script>-->
    <script src="{{asset('dashboard/js/bootstrap.min.js')}}"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
     <script src="{{asset('dashboard/js/bootstrap-checkbox-radio-switch.js')}}"></script>

	<!--  Charts Plugin -->
    <script src="{{asset('dashboard/js/chartist.min.js')}}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{asset('dashboard/js/bootstrap-notify.js')}}"></script>


    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

  <script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
     <!-- Date Range Picker -->
	<script src="{{asset('js/daterangepicker.js')}}"></script>

    <!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/i18n/defaults-*.min.js"></script>



</html>
