@extends('dashboard.layouts.master')
@section('content')
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                      @include('dashboard.messenger.partials.flash')
                      @if($user_type == 'Writer' && $papers->count() > 0)
                        <div class="alert alert-info" role="alert">
                            Dear {{Auth::user()->username}}, you need to accept the work you have been assigned in 10 Minutes. 
                        </div>
                     @endif
                    <div class="col-md-5">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Clients  ({{$clients->count()}})</h4>
                                <p class="category">Recently added</p>
                            </div>
                            <div class="content">
                               <div class="table-full-width">
                                   @if(!$clients->count())
                                      <div class="alert alert-info" role="alert">
                                           There are no Clients added in the Database
                                        </div>
                                   @else
                                    <table class="table">
                                        <tbody>
                                        <th>#</th>
                                        <th>Client Name</th>
                                        <th>Phone</th>
                                        <th>Location</th>
                                        <th>Date Added</th>
                                        <?php $count =1;?>
                                        @foreach($clients as $client)
                                            <tr>
                                                <td>{{$count}}</td>
                                                <td>{{$client->name}}</td>
                                                 <td>{{$client->phone}}</td>
                                                  <td>{{$client->location}}</td>
                                                <td><small>{{$client->created_at}}</small></td>
                                            </tr>
                                            <?php $count++;?>
                                        @endforeach  
                                         <tr><td>{{$clients->render()}}</td><tr>   
                                        </tbody>
                                    </table>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-7">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Paper Assignment Summary</h4>
                                <p class="category">No of Papers: {{$papers->count()}}</p>
                            </div>
                            <div class="content">
                                  <div class="table-full-width">
                                  @if(!$papers->count())
                                      <div class="alert alert-info" role="alert">
                                           There are Papers that have been registered
                                        </div>
                                   @else
                                    <table class="table">
                                        <tbody>
                                            <th>Paper Title</th>
                                            <th>Writer</th>
                                             <th>Due Date</th>
                                          
                                              @foreach($papers as $paper)
                                            <tr>
                                                <td>{{$paper->title}}</td>
                                                <td>{{$paper->name}}</td>
                                                <td><small>{{$paper->due_date}}</small></td>
                                                 <td class="td-actions text-right">
                                                      @if($user_type == 'Writer')
                                                        @if($paper->status == 'Accepted')
                                                        <span class="label label-success">{{$paper->status}}</span>
                                                        @elseif($paper->status == 'Rejected')
                                                        <span class="label label-danger">{{$paper->status}}</span>
                                                         @else
                                                            <a href="{{route('show-paper',$paper->paper_id)}}">
                                                                <button type="button" rel="tooltip" title="Accept Work" 
                                                                        class="btn btn-info btn-simple btn-xs"><p class ="label label-warning" id="demo"></p>
                                                                <i class="fa fa-edit"></i>Accept
                                                            </button></a>
                                                            <button type="button" rel="tooltip" title="Reject work" class="btn btn-danger btn-simple btn-xs">
                                                                <i class="fa fa-times"></i>Reject
                                                            </button>
                                                        @endif
                                                    @else
                                                     <td class="td-actions text-right">
                                                        @if($paper->status == 'Assigned')
                                                         <span class="label label-warning">{{$paper->status}}</span>
                                                         @elseif($paper->status == 'Accepted')
                                                         <span class="label label-success">{{$paper->status}}</span>
                                                         @else
                                                         <span class="label label-danger">{{$paper->status}}</span>
                                                        @endif
                                                    </td>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach   
                                        <tr><td>{{$papers->render()}}</td><tr> 
                                        </tbody>
                                    </table>
                                  @endif
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                </div>
            </div>
        </div>

@endsection

@section('javascript')

<script>
// Set the date we're counting down to

var countDownDate = new Date();
countDownDate.setMinutes(countDownDate.getMinutes() + 10);



// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("demo").innerHTML = minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("demo").innerHTML = "EXPIRED";
  }
}, 1000);
</script>
@endsection