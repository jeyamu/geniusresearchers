@extends('dashboard.layouts.master')
@section('content')
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-8">
<div class="card">
<div class="header">
<h4 class="title">Edit {{$admin->name}}'s Info</h4>
</div>     
@include('dashboard.messenger.partials.flash')                    
<div class="content">
<form action ="{{url('/admin/update')}}" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
    <input type="hidden" name="id" value="{{ $admin->id }}"> 
      <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('name')?'has-error':''}}">
                <label>Name</label>
                <input type="text" class="form-control" value="{{$admin->name}}" name="name">
                @if($errors->has('name'))
                <span class="label label-danger">{{ $errors->first('name') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('phone')?'has-error':''}}">
                <label>Phone</label>
                <input type="text" class="form-control" name="phone" value="{{$admin->phone}}">
                 @if($errors->has('phone'))
                <span class="label label-danger">{{ $errors->first('phone') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

    <div class="row">
         <div class="col-md-6">
            <div class="form-group  {{$errors->has('gender')?'has-error':''}}">
                <label>Gender</label>
                <input type="radio" name="gender" value="Female">Female
                 <input type="radio" name="gender" value="Male">Male
                @if($errors->has('gender'))
                <span class="label label-danger">{{ $errors->first('gender') }}
                </span>    
                @endif
            </div>
        </div>
    </div>  
    
    <input type="submit" name='register' class="btn btn-success" value = "Save"/>
    <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
    <div class="clearfix"></div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection