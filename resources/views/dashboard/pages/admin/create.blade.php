@extends('dashboard.layouts.master')
@section('content')
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-8">
<div class="card">
<div class="header">
<h4 class="title">Registration of New Admin</h4>
</div>     
@include('dashboard.messenger.partials.flash')                    
<div class="content">
<form action ="{{url('/admin/new')}}" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">                                 
      <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('firstname')?'has-error':''}}">
                <label>First Name</label>
                <input type="text" class="form-control" placeholder="First Name" name="firstname">
                @if($errors->has('firstname'))
                <span class="label label-danger">{{ $errors->first('firstname') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('lastname')?'has-error':''}}">
                <label>Last Name</label>
                <input type="text" class="form-control" placeholder="Last Name" name="lastname">
                 @if($errors->has('lastname'))
                <span class="label label-danger">{{ $errors->first('lastname') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('password')?'has-error':''}}">
                <label>Password</label>
                <input type="password" class="form-control" name="password">
                @if($errors->has('password'))
                <span class="label label-danger">{{ $errors->first('password') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('password_confirmation')?'has-error':''}}">
                <label>Re-enter Password</label>
                <input type="password" class="form-control" name="password_confirmation">
                 @if($errors->has('password_confirmation'))
                <span class="label label-danger">{{ $errors->first('password_confirmation') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('gender')?'has-error':''}}">
                <label>Gender</label>
                <input type="radio" name="gender" value="Female">Female
                 <input type="radio" name="gender" value="Male">Male
                @if($errors->has('gender'))
                <span class="label label-danger">{{ $errors->first('gender') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('photo')?'has-error':''}}">
                <label>Photo</label>
                <input type="file" name="photo" class="form-control">
                @if($errors->has('photo'))
                <span class="label label-danger">{{ $errors->first('photo') }}
                </span>    
                @endif
            </div>
        </div>
    </div>
       

     <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('email')?'has-error':''}}">
                <label>Email</label>
                <input type="email" class="form-control" name="email" placeholder="Email">
                @if($errors->has('email'))
                <span class="label label-danger">{{ $errors->first('email') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('phone')?'has-error':''}}">
                <label>Phone Number</label>
                <input type="text" class="form-control" name="phone" placeholder="Phone Number">
                 @if($errors->has('phone'))
                <span class="label label-danger">{{ $errors->first('phone') }}
                </span>    
                @endif
            </div>
        </div>
    </div>
  
    <input type="submit" name='register' class="btn btn-success" value = "Register"/>
    <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
    <div class="clearfix"></div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection