@extends('dashboard.layouts.master')
@section('content')
             <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                       @include('dashboard.messenger.partials.flash') 
                       @if(Auth::user()->role=='Admin')
                        <div class="card">
                            <div class="header">
                                 <h4 class="title"> <a href="{{route('create-writer')}}"><input type="submit" name='save' class="btn btn-info" value = "New Writer" /></a></h4>
                            </div>
							 @if ( !$writers->count() )
							  <div class="alert alert-info">
                                    <button type="button" aria-hidden="true" class="close">×</button>
                                    <span>There are no registered Writers currently.</span>
                                </div>                              
                            @else
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Name</th>
                                    	<th>Code</th>
                                    	<th>Residence</th>
                                    	<th>Skill Level</th>
										<th>Qualification</th>
                                        <th>Operations</th>
                                    </thead>
                                    <tbody>
										<?php $count =1;?>
										@foreach($writers as $writer)
                                        <tr>
                                        	<td>{{$count}}</td>
                                        	<td><a href="{{route('show-writer',$writer->writer_code)}}">{{$writer->name}}</a></td>
                                        	<td>{{$writer->writer_code}}</td>
                                        	<td>{{$writer->place_of_residence}}</td>
                                        	<td>{{$writer->skill_level}}</td>
											<td>{{$writer->qualification}}</td>
                                            <td> <a href="#"  data-toggle="modal" data-target="#assign_paper" data-id="{{$writer->id}}" data-name="{{$writer->name}}">
                                                              <i class="pe-7s-news-paper"  
                                                              data-toggle="tooltip" data-placement="top" title="Assign a paper to this Writer"></i></a>
                                               <a href="{{route('update.skill')}}" 
                                                     data-toggle="modal" data-target="#skill_level" data-id="{{$writer->id}}" data-name="{{$writer->name}}">
                                                      <i class="pe-7s-exapnd2" data-toggle="tooltip" 
                                                      data-placement="top" title="Update Writers Skill Level"></i></a>
                                            </td>  
                                           
                                             </tr>
										<?php $count++;?>
										@endforeach
                                    </tbody>
                                </table>
                            </div>
							@endif
                        @else
                          <?php
                          //get all the papers by the logged in Writer
                           $papers = \DB::table('papers')
                                    ->join('papers_for_preview', 'papers.id', '=', 'papers_for_preview.paper_id')
                                    ->join('writers','writers.id','=','papers_for_preview.paperable_id')
                                    ->select('papers.*', 'papers_for_preview.*','writers.*')
                                    ->where('writers.user_id','=',Auth::user()->id)    
                                    ->orderBy('papers_for_preview.id', 'desc')
                                    ->get();
                          ?>
                          @if(!$papers->count())
                            <div class="alert alert-info">
                                    <button type="button" aria-hidden="true" class="close">×</button>
                                    <span>Hello {{Auth::user()->username}}, you have no work assigned to you yet.</span>
                                </div>   
                         @else
                          <table class="display table table-hover" cellspacing="0" width="80%">
                             <thead>  
                                <th>Paper</th>  
                                <th>No of Pages</th>  
                                <th>Due date</th>  
                                <th>Status</th>  
                            <th>Date Assigned</th>  
                            </thead>
                            <tbody>
                            @foreach($papers as $paper)
                                <tr>
                                <td><a href="">{{$paper->title}}</a></td>
                                <td>{{number_format($paper->number_of_pages)}}</td>
                                <td>{{$paper->due_date}}</td>
                                <td> <label class= "label label-info">{{$paper->status}}</label></td>
                                    <td>{{$paper->created_at}}</td>
                            </tr> 
                            @endforeach                                           
                            
                            </tbody>
                          </table>
                          @endif                          
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

<!-- Modal -->
<div id="skill_level" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
       <form action ="{{url('/writer/change-skill_level')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">           
        <input type="hidden" name="writer_id" value="" id="writer_id">
        <div class="row">
            <div class="col-md-12">
               <div class="form-group  {{$errors->has('skill_level')?'has-error':''}}">
                <label>Skill Level</label>
                <select name = "skill_level"class="form-control" />
                   <option value ="">----Select Skill Level---</option>
                   <option value= "Freshman">Freshman</option>
                   <option value ="Junior">Junior</option>
                   <option value ="Intermediate">Intermediate</option>
                   <option value ="Expert">Expert</option>
                </select>
                @if($errors->has('skill_level'))
                <span class="label label-danger">{{ $errors->first('skill_level') }}
                </span>    
                @endif
            </div>
        </div>
        </div>
       
        <input type="submit" name='save' class="btn btn-success" value = "Update Skill Level"/>
        <input type="reset" name='reset' class="btn btn-default" value = "Cancel" />
        <div class="clearfix"></div>
    </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--ENd Update Skil Level Modal-->

<!-- assign_paper -->
<div id="assign_paper" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        @if (count($papers) < 1)
            <div class="alert alert-warning">
                <strong>Whoops!</strong> You need to <a href="{{route('create-paper')}}">Register</a> a paper first inorder to assign a writer.<br><br>
            </div>  
        @else 
        <form action ="{{url('/writer/assign-paper')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="writer_id" value="" id="writer_id">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group  {{$errors->has('paper')?'has-error':''}}">
                    <label>Paper to Assign Writer</label>
                    <select name = "paper"class="form-control" />
                    <option value ="">----Select Paper---</option>
                        @foreach($papers as $paper)
                        <option value ="{{$paper->id}}">{{$paper->title}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('paper'))
                    <span class="label label-danger">{{ $errors->first('paper') }}
                    </span>    
                    @endif
                </div>
            </div>
             <div class="col-md-6">
            <div class="form-group  {{$errors->has('due_date')?'has-error':''}}">
                <label>Due Date</label>
                <input type="date" class="form-control" name="due_date" id="due_date">
                @if($errors->has('due_date'))
                <span class="label label-danger">{{ $errors->first('due_date') }}
                </span>    
                @endif
            </div>
        </div>
        </div>
       
        <input type="submit" name='Assign Paper' class="btn btn-success" value = "Assign Paper"/>
        <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
        <div class="clearfix"></div>
    </form>
    @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--/assign_paper -->
@endsection

@section('javascript')
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
  $('#skill_level').on('show.bs.modal', function (event) { // id of the modal with event
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var billername = button.data('name')
   
    var title = "Updating "+ billername +"'s Skill Level"
    // Update the modal's content.
    var modal = $(this)
    modal.find('.modal-title').text(title)
    // And if you wish to pass the productid to modal's 'Yes' button for further processing
    modal.find('.modal-body #writer_id').val(id)
  })

   $('#assign_paper').on('show.bs.modal', function (event) 
   { // id of the modal with event
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var billername = button.data('name')
   
    var title = "Assigning Paper to Writer "+ billername
    // Update the modal's content.
    var modal = $(this)
    modal.find('.modal-title').text(title)
    // And if you wish to pass the productid to modal's 'Yes' button for further processing
    modal.find('.modal-body #writer_id').val(id)
  })
</script>


<script type="text/javascript">
 $(document).ready(function () {
  $('#due_date').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
         locale:{
            format: 'YYYY-MM-DD'
        }
    });
 });
</script>

<script type="text/javascript">
  $('select').select2();
</script>
@endsection

