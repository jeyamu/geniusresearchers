@extends('dashboard.layouts.master')
@section('content')
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-8">
<div class="card">
<div class="header">
<h4 class="title">Edit {{$writer->name}}'s Info</h4>
</div>     
@include('dashboard.messenger.partials.flash')                    
<div class="content">
<form action ="{{url('/writer/update')}}" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
    <input type="hidden" name="id" value="{{ $writer->id }}"> 
      <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('name')?'has-error':''}}">
                <label>Name</label>
                <input type="text" class="form-control" value="{{$writer->name}}" name="name">
                @if($errors->has('name'))
                <span class="label label-danger">{{ $errors->first('name') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('nin')?'has-error':''}}">
                <label>National ID No</label>
                <input type="text" class="form-control" name="nin" value="{{$writer->nin}}">
                 @if($errors->has('nin'))
                <span class="label label-danger">{{ $errors->first('nin') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('place_of_residence')?'has-error':''}}">
                <label>Residence</label>
                <input type="text" class="form-control" name="place_of_residence" value="{{$writer->place_of_residence}}">
                @if($errors->has('place_of_residence'))
                <span class="label label-danger">{{ $errors->first('place_of_residence') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('nationality')?'has-error':''}}">
                <label>Nationality</label>
                <select name = "nationality"class="form-control" />
                   <option value ="">----Select Nationality---</option>
                   <option value ="Uganda">Uganda</option>
                   <option value ="Kenya">Kenya</option>
                   <option value ="Tanzania">Tanzania</option>
                    <option value ="Rwanda">Rwanda</option>
                    <option value ="Burundi">Burundi</option>
                    <option value ="South Sudan">South Sudan</option>
                </select>
                @if($errors->has('nationality'))
                <span class="label label-danger">{{ $errors->first('nationality') }}
                </span>    
                @endif
            </div>
        </div>
    </div>


     <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('gender')?'has-error':''}}">
                <label>Gender</label>
                <input type="radio" name="gender" value="Female">Female
                 <input type="radio" name="gender" value="Male">Male
                @if($errors->has('gender'))
                <span class="label label-danger">{{ $errors->first('gender') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('date_of_birth')?'has-error':''}}">
                <label>Date Of Birth</label>
                <input type="date" class="form-control" name="date_of_birth" value="{{$writer->date_of_birth}}">
                 @if($errors->has('date_of_birth'))
                <span class="label label-danger">{{ $errors->first('date_of_birth') }}
                </span>    
                @endif
            </div>
        </div>
    </div>
     <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('skill_level')?'has-error':''}}">
                <label>Skill Level</label>
                <select name = "skill_level"class="form-control" />
                 <option value ="">----Select Skill Level---</option>
                   <option value ="Junior">Junior</option>
                   <option value ="Intermediate">Intermediate</option>
                   <option value ="Senior">Senior</option>
                </select>
                @if($errors->has('skill_level'))
                <span class="label label-danger">{{ $errors->first('skill_level') }}
                </span>    
                @endif
            </div>
        </div>

         <div class="col-md-6">
            <div class="form-group  {{$errors->has('qualification')?'has-error':''}}">
                <label>Qualification</label>
                <input type="text" class="form-control" name="qualification" value="{{$writer->qualification}}">
                 @if($errors->has('qualification'))
                <span class="label label-danger">{{ $errors->first('qualification') }}
                </span>    
                @endif
            </div>
        </div>
    </div>
    <input type="submit" name='register' class="btn btn-success" value = "Save"/>
    <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
    <div class="clearfix"></div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection