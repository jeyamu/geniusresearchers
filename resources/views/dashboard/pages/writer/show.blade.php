@extends('dashboard.layouts.master')
@section('content')

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">{{$writer->name}}</h4>
                            </div>
                            @include('dashboard.messenger.partials.flash') 
                            <div class="content">
                                <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Details</a></li>
                                    <li><a data-toggle="tab" href="#tasks">Tasks</a></li>
                                              
                                </ul>   
                                 <div class="tab-content">
                                        <div id="home" class="tab-pane fade in active">
                                            <h4>
                                                Writer's Details
                                            </h4>
                                            <table class="display table table-hover" cellspacing="0" width="80%">
                                                <thead>            
                                                </thead>
                                                <tbody>
                                                 <tr>
                                                    <td><small>Name : </small> <b>{{$writer->name}}</b></td>
                                                </tr>
                                                <tr>
                                                    <td><small>Code : </small><b>{{$writer->writer_code}}</b></td>
                                                </tr>
                                                <tr>
                                                    <td><small>Nationality : </small><b>{{$writer->nationality}}</b></td>
                                                </tr>
                                                <tr>
                                                    <td><small>NIN : </small><b>{{$writer->nin}}</b></td>
                                                </tr>
                                                <tr>
                                                    <td><small>Skill Level : </small><b>{{$writer->skill_level}}</b></td>
                                                </tr>
                                                <tr>
                                                    <td><small>Qualification : </small><b>{{$writer->qualification}}</b></td>
                                                </tr>
                                                <tr>
                                                    <td><small>Residence : </small><b>{{$writer->place_of_residence}}</b></td>
                                                </tr>
                                                <tr>
                                                    <td><small>Gender : </small><b>{{$writer->gender}}</b></td>
                                                </tr>
                                                 <tr>
                                                    <td><small>Registered On : </small><b>{{$writer->created_at}}</b></td>
                                                </tr>
                                                 <td><a href="{{route('download-cv',$writer->id)}}">
                                                            <input type="submit" name='save' 
                                                                    class="btn btn-success" value = "Download Cv"/>
                                                            </a>
                                                    </td>                                              
                                                </tbody>
                                            </table>
                                        </div><!--/details-->

                                         <div id="tasks" class="tab-pane fade">
                                
                                          @if ( !$papers->count() )
                                            <div class="alert alert-warning">
                                                    <button type="button" aria-hidden="true" class="close">×</button>
                                                    <span>There are no papers assigned to {{$writer->name}} for review, Click <a href="{{route('assign-paper',$writer->writer_code)}}">here</a> to assign</span>
                                                </div>                              
                                            @else
                                            <h4>
                                               Tasks Assigned
                                            </h4>
                                            <table class="display table table-hover" cellspacing="0" width="80%">
                                                <thead>  
                                                 <th>Paper</th>  
                                                 <th>No of Pages</th>  
                                                 <th>Due date</th>  
                                                 <th>Status</th>  
                                                <th>Date Assigned</th>  
                                                </thead>
                                                <tbody>
                                                @foreach($papers as $paper)
                                                 <tr>
                                                    <td><a href="">{{$paper->title}}</a></td>
                                                    <td>{{number_format($paper->number_of_pages)}}</td>
                                                    <td>{{$paper->due_date}}</td>
                                                    <td> <label class= "label label-info">{{$paper->status}}</label></td>
                                                     <td>{{$paper->updated_at}}</td>
                                                </tr> 
                                                @endforeach   
                                                <tr>
                                                  <td><a href="{{route('assign-paper',$writer->writer_code)}}">
                                                            <input type="submit" name='save' 
                                                                    class="btn btn-info" value = "Assign More Work"/>
                                                            </a>
                                                    </td>   
                                                </tr>                                        
                                              
                                                </tbody>
                                            </table>
                                          @endif
                                        </div><!--/Tasks-->

                                        </div> <!--/Tab Content-->


                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-user">
                            <div class="image">
                                <img src="{{asset($photo->path)}}" alt="..."/>
                            </div>
                            <div class="content">
                                <div class="author">
                                     <a href="#">
                                   
                                      <h4 class="title">{{$writer->name}}<br />
                                         <small></small>
                                      </h4>
                                    </a>
                                </div>
                               
                            </div>
                            <hr>
                            <div class="text-center">
                                <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
@endsection

