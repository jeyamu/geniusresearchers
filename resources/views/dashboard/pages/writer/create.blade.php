@extends('dashboard.layouts.master')
@section('content')
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-8">
<div class="card">
<div class="header">
<h4 class="title">Registration of New Writer</h4>
</div>     
@include('dashboard.messenger.partials.flash')                    
<div class="content">
<form action ="{{url('/writer/new')}}" method="POST" enctype="multipart/form-data" autocomplete="false">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">                                 
      <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('firstname')?'has-error':''}}">
                <label>First Name</label>
                <input type="text" class="form-control" 
                value="{{old('firstname')}}"
                placeholder="First Name" name="firstname">
                @if($errors->has('firstname'))
                <span class="label label-danger">{{ $errors->first('firstname') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('lastname')?'has-error':''}}">
                <label>Last Name</label>
                <input type="text" class="form-control" 
                   value="{{old('lastname')}}"
                   placeholder="Last Name" name="lastname">
                 @if($errors->has('lastname'))
                <span class="label label-danger">{{ $errors->first('lastname') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('password')?'has-error':''}}">
                <label>Password</label>
                <input type="password" class="form-control" name="password">
                @if($errors->has('password'))
                <span class="label label-danger">{{ $errors->first('password') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('password_confirmation')?'has-error':''}}">
                <label>Re-enter Password</label>
                <input type="password" class="form-control" name="password_confirmation">
                 @if($errors->has('password_confirmation'))
                <span class="label label-danger">{{ $errors->first('password_confirmation') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('email')?'has-error':''}}">
                <label>Email</label>
                <input type="email" class="form-control" 
                value="{{old('email')}}"
                name="email" placeholder="Email">
                @if($errors->has('email'))
                <span class="label label-danger">{{ $errors->first('email') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('nin')?'has-error':''}}">
                <label>National ID No</label>
                <input type="text" class="form-control" 
                value="{{old('nin')}}"               
                name="nin" placeholder="NIN">
                 @if($errors->has('nin'))
                <span class="label label-danger">{{ $errors->first('nin') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('place_of_residence')?'has-error':''}}">
                <label>Residence</label>
                <input type="text" class="form-control" 
                value="{{old('place_of_residence')}}"
                name="place_of_residence" placeholder="Residence">
                @if($errors->has('place_of_residence'))
                <span class="label label-danger">{{ $errors->first('place_of_residence') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('nationality')?'has-error':''}}">
                <label>Nationality</label>
                <select name = "nationality"class="form-control" />
                   <option value ="">----Select Nationality---</option>
                   <option value ="Uganda">Uganda</option>
                   <option value ="Kenya">Kenya</option>
                   <option value ="Tanzania">Tanzania</option>
                    <option value ="Rwanda">Rwanda</option>
                    <option value ="Burundi">Burundi</option>
                    <option value ="South Sudan">South Sudan</option>
                </select>
                @if($errors->has('nationality'))
                <span class="label label-danger">{{ $errors->first('nationality') }}
                </span>    
                @endif
            </div>
        </div>
    </div>


     <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('gender')?'has-error':''}}">
                <label>Gender</label>
                <input type="radio" name="gender" value="Female">Female
                 <input type="radio" name="gender" value="Male">Male
                @if($errors->has('gender'))
                <span class="label label-danger">{{ $errors->first('gender') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('date_of_birth')?'has-error':''}}">
                <label>Date Of Birth</label>
                <input type="date" class="form-control" id="date_of_birth" name="date_of_birth" value="{{old('date_of_birth')}}">
                 @if($errors->has('date_of_birth'))
                <span class="label label-danger">{{ $errors->first('date_of_birth') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

    
     <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('cv')?'has-error':''}}">
                <label>Resume/CV</label>
               <input value="{{ old('cv') }}"  type="file" name = "cv"class="form-control" />
                @if($errors->has('cv'))
                <span class="label label-danger">{{ $errors->first('cv') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('photo')?'has-error':''}}">
                <label>Photo</label>
                <input value="{{ old('photo') }}"  type="file" name = "photo"class="form-control" />
                @if($errors->has('photo'))
                <span class="label label-danger">{{ $errors->first('photo') }}
                </span>    
                @endif
            </div>
        </div>
    </div>
     <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('skill_level')?'has-error':''}}">
                <label>Skill Level</label>
                <select name = "skill_level"class="form-control" />
                   <option value ="">----Select Skill Level---</option>
                   <option value= "Freshman">Freshman</option>
                   <option value ="Junior">Junior</option>
                   <option value ="Intermediate">Intermediate</option>
                   <option value ="Expert">Expert</option>
                </select>
                @if($errors->has('skill_level'))
                <span class="label label-danger">{{ $errors->first('skill_level') }}
                </span>    
                @endif
            </div>
        </div>

         <div class="col-md-6">
            <div class="form-group  {{$errors->has('qualification')?'has-error':''}}">
                <label>Qualification</label>
                <input type="text" class="form-control"
                 value="{{old('qualification')}}"
                 name="qualification" placeholder="B.A Literature">
                 @if($errors->has('qualification'))
                <span class="label label-danger">{{ $errors->first('qualification') }}
                </span>    
                @endif
            </div>
        </div>
    </div>
    <input type="submit" name='register' class="btn btn-success" value = "Register"/>
    <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
    <div class="clearfix"></div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
    <!-- Date Range Picker -->
<script src="{{asset('js/daterangepicker.js')}}" type="text/javascript"></script>

<script type="text/javascript">
 $(document).ready(function () {
  $('#date_of_birth').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
         locale:{
            format: 'YYYY-MM-DD'
        }
    });
 });
</script>
@endsection