@extends('dashboard.layouts.master')
@section('content')
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-8">
<div class="card">
<div class="header">
<h4 class="title">Assigning Work to Writer</h4>
<h5 class="title">Writer Name:  {{$writer->name}}</h5>
<h5 class="title">Writer Skill Level:  {{$writer->skill_level}}</h5>
<h5 class="title">Writer Code:  {{$writer->writer_code}}</h5>

</div>     
@include('dashboard.messenger.partials.flash')             
<div class="content">
@if (count($papers) < 1)
<div class="alert alert-warning">
    <strong>Whoops!</strong> You need to <a href="{{route('create-paper')}}">Register</a> a paper first inorder to assign a writer.<br><br>
</div>  
@else 
<form action ="{{url('/writer/assign-paper')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">           
        <input type="hidden" name="writer_id"  value="{{$writer->id}}">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group  {{$errors->has('paper')?'has-error':''}}">
                    <label>Paper to Assign Writer</label>
                    <select name = "paper"class="form-control" />
                    <option value ="">----Select Paper---</option>
                        @foreach($papers as $paper)
                        <option value ="{{$paper->id}}">{{$paper->title}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('paper'))
                    <span class="label label-danger">{{ $errors->first('paper') }}
                    </span>    
                    @endif
                </div>
            </div>
             <div class="col-md-6">
            <div class="form-group  {{$errors->has('due_date')?'has-error':''}}">
                <label>Due Date</label>
                <input type="date" class="form-control" name="due_date" id="due_date">
                @if($errors->has('due_date'))
                <span class="label label-danger">{{ $errors->first('due_date') }}
                </span>    
                @endif
            </div>
        </div>
        </div>
       
        <input type="submit" name='Assign Paper' class="btn btn-success" value = "Assign Paper"/>
        <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
        <div class="clearfix"></div>
    </form>
@endif
</div>
</div>
</div>
</div>
</div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
    <!-- Date Range Picker -->
<script src="{{asset('js/daterangepicker.js')}}" type="text/javascript"></script>

<script type="text/javascript">
 $(document).ready(function () {
  $('#due_date').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
         locale:{
            format: 'YYYY-MM-DD'
        }
    });
 });
</script>
@endsection