@extends('dashboard.layouts.master')
@section('content')

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">{{$client->name}}</h4>
                            </div>
                            @include('dashboard.messenger.partials.flash') 
                            <div class="content">
                                <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Details</a></li>
                                    <li><a data-toggle="tab" href="#tasks">Papers</a></li>
                                    <li><a data-toggle="tab" href="#messages">Messages</a></li>            
                                </ul>   
                                 <div class="tab-content">
                                        <div id="home" class="tab-pane fade in active">
                                            <h4>
                                                Client's Details
                                            </h4>
                                            <table class="display table table-hover" cellspacing="0" width="80%">
                                                <thead>            
                                                </thead>
                                                <tbody>
                                                 <tr>
                                                    <td><small>Name : </small> <b>{{$client->name}}</b></td>
                                                </tr>                                               
                                              
                                                </tbody>
                                            </table>
                                        </div><!--/details-->

                                         <div id="tasks" class="tab-pane fade">
                                
                                          @if ( !$papers->count() )
                                            <div class="alert alert-warning">
                                                    <button type="button" aria-hidden="true" class="close">×</button>
                                                    <span>There are no papers owned by {{$client->name}} for review, <a href="{{route('create-paper')}}">Click</a> to assign paper to this Cleint</span>
                                                </div>                              
                                            @else
                                            <h4>
                                               Tasks Assigned
                                            </h4>
                                            <table class="display table table-hover" cellspacing="0" width="80%">
                                                <thead>  
                                                 <th>Paper</th>  
                                                 <th>No of Pages</th>  
                                                 <th>Due date</th>  
                                                 <th>Status</th>  
                                                <th>Date Assigned</th>  
                                                </thead>
                                                <tbody>
                                                @foreach($papers as $paper)
                                                 <tr>
                                                    <td><a href="">{{$paper->title}}</a></td>
                                                    <td>{{number_format($paper->number_of_pages)}}</td>
                                                    <td>{{$paper->due_date}}</td>
                                                    <td> <label class= "label label-info">{{$paper->status}}</label></td>
                                                     <td>{{$paper->created_at}}</td>
                                                </tr> 
                                                @endforeach                                           
                                              
                                                </tbody>
                                            </table>
                                          @endif
                                        </div><!--/Tasks-->

                                        </div> <!--/Tab Content-->


                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card card-user">
                             <div class="image">
                                <img src="{{asset($photo->path)}}" alt="..."/>
                            </div>
                             <div class="content">
                                <div class="author">
                                     <a href="#">
                                   
                                      <h4 class="title">{{$client->name}}<br />
                                         <small></small>
                                      </h4>
                                    </a>
                                </div>
                               
                            </div>
                            <hr>
                            <div class="text-center">
                                <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                                <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
@endsection