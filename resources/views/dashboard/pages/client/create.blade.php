@extends('dashboard.layouts.master')
@section('content')
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-8">
<div class="card">
<div class="header">
<h4 class="title">Registration of New Client</h4>
</div>            
@include('dashboard.messenger.partials.flash')             
<div class="content">
<form action ="{{url('/client/new')}}" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">                                 
      <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('firstname')?'has-error':''}}">
                <label>First Name</label>
                <input type="text" class="form-control" placeholder="First Name" name="firstname" value="{{old('firstname')}}">
                @if($errors->has('firstname'))
                <span class="label label-danger">{{ $errors->first('firstname') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('lastname')?'has-error':''}}">
                <label>Last Name</label>
                <input type="text" class="form-control" placeholder="Last Name" name="lastname" value="{{old('lastname')}}">
                 @if($errors->has('lastname'))
                <span class="label label-danger">{{ $errors->first('lastname') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

     <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('email')?'has-error':''}}">
                <label>Email</label>
                <input type="email" class="form-control" name="email" placeholder="Email" value="{{old('email')}}">
                @if($errors->has('email'))
                <span class="label label-danger">{{ $errors->first('email') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('phone')?'has-error':''}}">
                <label>Phone</label>
                <input type="text" class="form-control" name="phone" placeholder="Phone Contact" value="{{old('phone')}}">
                @if($errors->has('phone'))
                <span class="label label-danger">{{ $errors->first('phone') }}
                </span>    
                @endif
            </div>
        </div>  
    </div>  
     <div class="row">
         <div class="col-md-6">
            <div class="form-group  {{$errors->has('location')?'has-error':''}}">
                <label>Location</label>
                <input type="text" class="form-control" name="location" placeholder="Location" value="{{old('location')}}">
                 @if($errors->has('location'))
                <span class="label label-danger">{{ $errors->first('location') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('photo')?'has-error':''}}">
                <label>Photo</label>
                <input value="{{ old('photo') }}"  type="file" name = "photo"class="form-control" value="{{old('photo')}}"/>
                @if($errors->has('photo'))
                <span class="label label-danger">{{ $errors->first('photo') }}
                </span>    
                @endif
            </div>
        </div>
        </div>
        <div class="row">
         <div class="col-md-12">
            <div class="form-group  {{$errors->has('date_of_joining')?'has-error':''}}">
                <label>Date Joined</label>
                <input type="date" class="form-control" id="date_of_joining" name="date_of_joining" placeholder="Date Joined" value="{{old('date_of_joining')}}">
                 @if($errors->has('date_of_joining'))
                <span class="label label-danger">{{ $errors->first('date_of_joining') }}
                </span>    
                @endif
            </div>
        </div>
     </div>
    <input type="submit" name='register' class="btn btn-success" value = "Register"/>
    <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
    <div class="clearfix"></div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection
@section('javascript')
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
    <!-- Date Range Picker -->
<script src="{{asset('js/daterangepicker.js')}}" type="text/javascript"></script>
<script type="text/javascript">
 $(document).ready(function () {
  $('#date_of_joining').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
         locale:{
            format: 'YYYY-MM-DD'
        }
    });
 });
</script>

@endsection