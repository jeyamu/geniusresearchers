@extends('dashboard.layouts.master')
@section('content')
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-8">
<div class="card">
<div class="header">
<h4 class="title">Edit {{$client->name}}'s Info</h4>
</div>     
@include('dashboard.messenger.partials.flash')                    
<div class="content">
<form action ="{{url('/client/update')}}" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}"> 
    <input type="hidden" name="id" value="{{ $client->id }}"> 
      <div class="row">
        <div class="col-md-12">
            <div class="form-group  {{$errors->has('name')?'has-error':''}}">
                <label>Name</label>
                <input type="text" class="form-control" value="{{$client->name}}" name="name">
                @if($errors->has('name'))
                <span class="label label-danger">{{ $errors->first('name') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('email')?'has-error':''}}">
                <label>Email</label>
                <input type="email" class="form-control" name="email" value="{{$client->email}}"">
                @if($errors->has('email'))
                <span class="label label-danger">{{ $errors->first('email') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('phone')?'has-error':''}}">
                <label>Phone</label>
                <input type="text" class="form-control" name="phone"
                       value="{{$client->phone}}">
                @if($errors->has('phone'))
                <span class="label label-danger">{{ $errors->first('phone') }}
                </span>    
                @endif
            </div>
        </div>  
    </div>  
     <div class="row">
         <div class="col-md-6">
            <div class="form-group  {{$errors->has('location')?'has-error':''}}">
                <label>Location</label>
                <input type="text" class="form-control" name="location" value="{{$client->location}}">
                 @if($errors->has('location'))
                <span class="label label-danger">{{ $errors->first('location') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('date_of_joining')?'has-error':''}}">
                <label>Date Joined</label>
                <input type="date" class="form-control" name="date_of_joining" value="{{$client->date_of_joining}}">
                 @if($errors->has('date_of_joining'))
                <span class="label label-danger">{{ $errors->first('date_of_joining') }}
                </span>    
                @endif
            </div>
         </div>
        </div>    
    <input type="submit" name='register' class="btn btn-success" value = "Save"/>
    <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
    <div class="clearfix"></div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection