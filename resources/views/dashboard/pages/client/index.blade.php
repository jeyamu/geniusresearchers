@extends('dashboard.layouts.master')
@section('content')
             <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                 <h4 class="title"> <a href="{{route('create-client')}}"><input type="submit" name='save' class="btn btn-info" value = "New Client" /></a></h4>
                            </div>
                            @include('dashboard.messenger.partials.flash') 
							 @if ( !$clients->count() )
							  <div class="alert alert-info">
                                    <button type="button" aria-hidden="true" class="close">×</button>
                                    <span>There are no registered Clients currently.</span>
                                </div>                              
                            @else
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Name</th>
                                    	<th>Phone No</th>
                                    	<th>Email</th>
                                    	<th>Residence</th>
										<th>Date Joined</th>
                                        <th>Date Registered</th>
                                    </thead>
                                    <tbody>
										<?php $count =1;?>
										@foreach($clients as $client)
                                        <tr>
                                        	<td>{{$count}}</td>
                                        	<td><a href="{{route('client',$client->id)}}">{{$client->name}}</a></td>
                                        	<td>{{$client->phone}}</td>
                                        	<td>{{$client->email}}</td>
                                        	<td>{{$client->location}}</td>
											<td>{{$client->date_of_joining}}</td>
                                            <td>{{$client->created_at}}</td>
                                        </tr>
										<?php $count++;?>
										@endforeach
                                    </tbody>
                                </table>
                            </div>
							@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection