@extends('dashboard.layouts.master')
@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div class="card">
    <div class="header">
        <h4 class="title"> <a href="{{route('create-article')}}"><input type="submit" name='save' class="btn btn-info" value = "Create Article" /></a></h4>
    </div>
    <div class="content">
	@include('dashboard.messenger.partials.flash') 

       @if ( !$posts->count() )
There is no post till now. Login and write a new post now!!!
@else
<div class="">
	@foreach( $posts as $post )
	<div class="list-group">
		<div class="list-group-item">
			<h5><a href="{{ url('article/edit/'.$post->slug) }}">{{ $post->title }}</a>
				@if(!Auth::guest() && ($post->author_id == Auth::user()->id || Auth::user()->is_admin()))
					@if($post->active == '1')
					<button class="btn" style="float: right"><a href="{{ url('article/edit/'.$post->slug)}}">Edit Post</a></button>
					@else
					<button class="btn" style="float: right"><a href="{{ url('article/edit/'.$post->slug)}}">Edit Draft</a></button>
					@endif
				@endif
			</h5>
			<small>{{ $post->created_at->format('M d,Y \a\t h:i a') }} By <a href="{{ url('/user/'.$post->author_id)}}">{{ $post->author->name }}</a></small>
			
		</div>
		<div class="list-group-item">
			<article>
				{!! str_limit($post->body, $limit = 1500, $end = '....... <a href='.url("/".$post->slug).'>Read More</a>') !!}
			</article>
		</div>
	</div>
	@endforeach
	{!! $posts->render() !!}
</div>
@endif
</div>
</div>
</div>

</div>
</div>
</div>
@endsection