@extends('dashboard.layouts.master')
@section('content')
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Articles for Blog</h4>
                            </div>
                            @include('dashboard.messenger.partials.flash') 
                            <div class="content">
                                <form action ="/article/update" method="post">
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input type="hidden" name="post_id" value="{{ $post->id }}{{ old('post_id') }}">       
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Title</label>
                                              <input required="required" placeholder="Enter title here" type="text" name = "title" class="form-control" value="@if(!old('title')){{$post->title}}@endif{{ old('title') }}"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Article</label>                                
                                                <textarea name='body'class="form-control">
                                                    @if(!old('body'))
                                                    {!! $post->body !!}
                                                    @endif
                                                    {!! old('body') !!}
                                                </textarea>
                                            </div>
                                        </div>
                                    </div>
                                     
                                    @if($post->active == '1')
                <input type="submit" name='publish' class="btn btn-success" value = "Update"/>
                @else
                <input type="submit" name='publish' class="btn btn-success" value = "Publish"/>
                @endif
                <input type="submit" name='save' class="btn btn-default" value = "Save As Draft" />
                <a href="{{  url('delete/'.$post->id.'?_token='.csrf_token()) }}" class="btn btn-danger">Delete</a>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection