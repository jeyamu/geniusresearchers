@extends('dashboard.layouts.master')
@section('content')
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Articles for Blog</h4>
                            </div>    
                            @include('dashboard.messenger.partials.flash')                     
                            <div class="content">
                                <form action ="{{url('/article/new')}}" method="POST" enctype="multipart/form-data">
                                   <input type="hidden" name="_token" value="{{ csrf_token() }}">                                 
                                    
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group {{$errors->has('title')?'has-error':''}}">
                                                <label>Title</label>
                                               <input required="required" value="{{ old('title') }}" placeholder="Enter title here" type="text" name = "title"class="form-control" />
                                              @if($errors->has('title'))
                                                <span class="label label-danger">{{ $errors->first('title') }}
                                                </span>    
                                              @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group  {{$errors->has('body')?'has-error':''}}">
                                                <label>Article</label>                                
                                                <textarea name='body' rows="5" placeholder="Describe the Article here" class="form-control">{{ old('body') }}</textarea>
                                                @if($errors->has('body'))
                                                <span class="label label-danger">{{ $errors->first('body') }}
                                                </span>    
                                              @endif
                                            </div>
                                        </div>
                                    </div>
                                     <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group  {{$errors->has('photo')?'has-error':''}}">
                                                <label>Photo</label>
                                               <input value="{{ old('photo') }}"  type="file" name = "photo"class="form-control" />
                                               @if($errors->has('photo'))
                                                <span class="label label-danger">{{ $errors->first('photo') }}
                                                </span>    
                                              @endif
                                            </div>
                                        </div>
                                    </div>


                                   <input type="submit" name='publish' class="btn btn-success" value = "Publish"/>
	                               <input type="submit" name='save' class="btn btn-default" value = "Save Draft" />
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection