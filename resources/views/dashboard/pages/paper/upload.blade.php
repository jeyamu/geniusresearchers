@extends('dashboard.layouts.master')
@section('content')

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="header">
                               <h4 class="title">{{$paper->title}}</h4>
                            </div>
                            @include('dashboard.messenger.partials.flash') 
                            <div class="content"> 
                                <table class="display table table-hover" cellspacing="0" width="80%">
                                                <thead>            
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Due Date : </small><b>{{$paper->due_date}}</b></td>
                                                </tr>
                                                <tr>
                                                    <td>Number of Pages :<b>{{$paper->number_of_pages}}</b></td>
                                                </tr>                                                                              
                                              
                                                </tbody>
                                            </table>
                             <div class="tab-content">
                                <form action ="{{url('/paper/upload-work')}}" method="POST" enctype="multipart/form-data">
                                <div class="row"> 
                                      <input type="hidden" name="_token" value="{{ csrf_token() }}">  
                                        <input type="hidden" name="paper_id" value="{{$paper->id}}">                                         
                                   <div class="col-md-6">
                                                <div class="form-group  {{$errors->has('document')?'has-error':''}}">
                                                    <label>Upload Your Work</label>
                                                    <input value="{{ old('document') }}"  type="file" name = "document"class="form-control" value="{{old('document')}}" />
                                                    @if($errors->has('document'))
                                                    <span class="label label-danger">{{ $errors->first('document') }}
                                                    </span>    
                                                    @endif
                                                </div>
                                            </div>                          
                                        </div> <!--/row--> 
                                    <input type="submit" name='register' class="btn btn-success" value = "Upload your work"/>
                                    <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
                                    <div class="clearfix"></div>
                                </form>                          
                            </div>                            
                        </div>                        
                    </div>
                    </div>

                     
                </div>
            </div>
        </div>
@endsection
