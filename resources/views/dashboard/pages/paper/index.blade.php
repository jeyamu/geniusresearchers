@extends('dashboard.layouts.master')
@section('content')
             <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            @if(Auth::user()->role == 'Admin')
                            <div class="header">
                                 <h4 class="title"> <a href="{{route('create-paper')}}"><input type="submit" name='save' class="btn btn-info" value = "New Paper" /></a></h4>
                            </div>
                            @else
                             <div class="header">
                              <h4 class="title">My Papers</h4>
                              </div>
                            @endif
                            @include('dashboard.messenger.partials.flash')    
							 @if ( !$papers->count() )
							  <div class="alert alert-info">
                                    <button type="button" aria-hidden="true" class="close">×</button>
                                    <span>There are no registered Papers currently.</span>
                                </div>                              
                            @else
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Title</th>
                                    	<th>Date Received</th>
                                        <th>Due Date </th>
                                    	<th>Instructions</th>
										<th>Status</th>
                                        <th>Actions</th>
                                    </thead>
                                    <tbody>
										<?php $count =1;?>
										@foreach($papers as $paper)
                                        <tr>
                                        	<td>{{$count}}</td>
                                           	<td><a href="{{route('show-paper',$paper->id)}}">{{$paper->title}}</a></td>
                                            <td>{{$paper->date_received}}</td>
                                            <td>{{$paper->due_date}}</td>
                                        	<td>{{$paper->instructions}}</td>
                                            <td> @if($paper->status == 'Assigned')
                                                <span class="label label-warning">{{$paper->status}}</span>
                                                @elseif($paper->status == 'Accepted')
                                                <span class="label label-success">{{$paper->status}}</span>
                                                @else
                                                <span class="label label-danger">{{$paper->status}}</span>
                                            @endif
                                            </td>            
											<td> 
                                               <a href="{{route('paper-download',$paper->id)}}"
                                                data-toggle="tooltip" data-placement="top" title="Download">
                                                <i class="pe-7s-download"></i></a>
                                                  @if(Auth::user()->role == 'Admin')
                                                <a href="" data-toggle="modal" data-target="#assign_writer" 
                                                     data-id="{{$paper->id}}" data-name="{{$paper->title}}">                                               
                                                      <i class="pe-7s-user" data-toggle="tooltip"
                                                  data-placement="top" title="Assign Writer"></i></a>                                                
                                               @endif
                                            </td>                                      
                                           
                                          </tr>
										<?php $count++;?>
										@endforeach
                                    </tbody>
                                </table>
                            </div>
							@endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>


<!-- assign_paper -->
<div id="assign_writer" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Assign Writer </h4>
      </div>
      <div class="modal-body">
        @if (count($writers) < 1)
            <div class="alert alert-warning">
                <strong>Whoops!</strong> You need to <a href="{{route('create-writer')}}">Register</a> a writer first inorder to assign a paper.<br><br>
            </div>  
        @else 
        <form action ="{{url('/paper/assign-writer')}}" method="POST" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="paper_id" value="" id="paper_id">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group  {{$errors->has('writer')?'has-error':''}}">
                    <label>Writer</label>
                    <select name = "writer"class="form-control" />
                    <option value ="">----Select Writer---</option>
                        @foreach($writers as $data)
                        <option value ="{{$data->id}}">{{$data->name}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('writer'))
                    <span class="label label-danger">{{ $errors->first('writer') }}
                    </span>    
                    @endif
                </div>
            </div>
          </div>
          <div class="row">
             <div class="col-md-12">
            <div class="form-group  {{$errors->has('due_date')?'has-error':''}}">
                <label>Due Date</label>
                <input type="date" class="form-control" name="due_date" id="due_date">
                @if($errors->has('due_date'))
                <span class="label label-danger">{{ $errors->first('due_date') }}
                </span>    
                @endif
            </div>
        </div>
        </div>
       
        <input type="submit" name='Assign Paper' class="btn btn-success" value = "Assign Writer"/>
        <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
        <div class="clearfix"></div>
    </form>
    @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--/assign_paper -->
@endsection


@section('javascript')
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
   $('#assign_writer').on('show.bs.modal', function (event) { // id of the modal with event
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes
    var name = button.data('name')
   
    var title = "Assigning Writer to, "+name
    // Update the modal's content.
    var modal = $(this)
    modal.find('.modal-title').text(title)
    // And if you wish to pass the productid to modal's 'Yes' button for further processing
    modal.find('.modal-body #paper_id').val(id)
  });
</script>


<script type="text/javascript">
 $(document).ready(function () {
  $('#due_date').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
         locale:{
            format: 'YYYY-MM-DD'
        }
    });
 });
</script>

<script type="text/javascript">
  $('select').select2();
</script>
@endsection

