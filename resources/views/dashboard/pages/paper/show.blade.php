    @extends('dashboard.layouts.master')
    @section('content')

    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">{{$paper->title}}</h4>
                             @if($paper->status == 'Assigned' && Auth::user()->role=='Writer')
                              <p class ="label label-warning" id="demo"></p>                                                   
                            @endif
                        </div>
                        @include('dashboard.messenger.partials.flash') 
                         <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Paper Details</a></li>
                                    <li><a data-toggle="tab" href="#writers">Assigned Writers</a></li>                
                          </ul>   
                        <div class="content"> 
                                <div class="tab-content">
                                    <div id="home" class="tab-pane fade in active">                                        
                                        <table class="display table table-hover" cellspacing="0" width="80%">
                                            <thead>            
                                            </thead>
                                            <tbody>
                                                <tr>
                                                <td>Title : <b>{{$paper->title}}</b></td>
                                            </tr>
                                            <tr>
                                                <td>Due Date : </small><b>{{$paper->due_date}}</b></td>
                                            </tr>
                                            <tr>
                                                <td>Number of Pages :<b>{{$paper->number_of_pages}}</b></td>
                                            </tr>
                                                <tr>
                                                                                                
                                                    @if($paper->status == 'Assigned' && Auth::user()->role=='Writer')
                                                    <td><td>
                                                 
                                                    <a href="{{route('paper-accept',$paper->id)}}">
                                                            <input type="submit" name='save' id="demo"
                                                                    class="btn btn-info" value = "Accept"/>
                                                                    
                                                            </a>
                                                    </td></td>
                                                    @elseif($paper->status == 'Accepted')
                                                    <td><td><a href="{{route('upload-paper',$paper->id)}}">
                                                            <input type="submit" name='save' 
                                                                    class="btn btn-success" value = "Upload Work"/>
                                                            </a>
                                                    </td></td>
                                                    @elseif($paper->status == 'Pending Approval' && Auth::user()->role == 'Admin')
                                                    <td><td><a href="{{route('upload-paper',$paper->id)}}">
                                                            <input type="submit" name='save' 
                                                                    class="btn btn-success" value = "Approve"/>
                                                            </a>
                                                    </td></td>
                                                    @elseif ($paper->status== 'Unassigned' && Auth::user()->role=='Admin')
                                                    <td><a href="{{route('writers')}}">
                                                            <input type="submit" name='save' 
                                                                    class="btn btn-success" value = "Assign to Writer"/>
                                                            </a>
                                                    </td>
                                                    @endif
                                            </tr>                                         
                                            
                                            </tbody>
                                        </table>
                                    </div><!--/details--> 
                                     <div id="writers" class="tab-pane fade">
                                            <h4>Number of Writers working on this paper: {{$writers->count() }} </h4>
                                             @if ( !$writers->count() )
                                            <div class="alert alert-warning">
                                                    <button type="button" aria-hidden="true" class="close">×</button>
                                                    <span>There are no Writers assigned to this paper.</span>
                                                </div>                              
                                            @else                                
                                            <table class="display table table-hover" cellspacing="0" width="80%">
                                                <thead>  
                                                <th>#</th>
                                                <th>Name</th>                                                  
                                                <th>Writer Code</th> 
                                                <th>NIN</th> 
                                                <th>Skill Level</th>  
                                                <th>Qualification</th> 
                                                <th>Gender</th>             
                                                </thead>
                                                <tbody>
                                                <?php $count =1;?>
                                                @foreach($writers as $writer)
                                                 <tr>
                                                    <td>{{$count}}</td>
                                                    <td><a href="{{route('show-writer',$writer->writer_code)}}">{{$writer->name}}</a></td>                                                   
                                                    <td>{{$writer->writer_code}}</td>
                                                    <td>{{$writer->nin}}</td>
                                                    <td>{{$writer->skill_level}}</td>
                                                    <td>{{$writer->qualification}}</td>
                                                    <td>{{$writer->gender}}</td>                                                   
                                                </tr> 
                                                <?php $count++;?>
                                                @endforeach                                          
                                              
                                                </tbody>

                                            </table>
                                            {{ $writers->links() }}
                                          @endif
                                        </div><!--/Writers-->                                       
                                </div> <!--/Tab Content-->                           
                        </div>                            
                    </div>                        
                </div>

                    <div class="col-md-4">
                    <div class="card card-user">                            
                        <div class="content">
                            <h5 class="title">Paper Instructions</h5>                                
                            <ul>
                                <li>
                            <p class="description text-left">  
                                {{$paper->instructions}}
                            </p>
                            @if($paper->status == 'Unassigned')
                            <a href="{{route('paper-download',$paper->id)}}">
                                 <input type="submit" name='save' class="btn btn-success" value = "Download More Instructions"/>
                                 </a>
                            @else
                             <a href="{{route('paper-download',$paper->id)}}">
                                 <input type="submit" name='save' class="btn btn-success" value = "Download Work"/>
                                 </a>
                            @endif
                            </li>
                            </ul>
                        </div>
                        <hr>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')

<script>
// Set the date we're counting down to

var countDownDate = new Date();
countDownDate.setMinutes(countDownDate.getMinutes() +1);



// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
  document.getElementById("demo").innerHTML ='Time left to Accept Paper '+ minutes + "m " + seconds + "s ";

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    //Assign the paper automatically
    document.getElementById("demo").innerHTML = "0s";
  }
}, 1000);
</script>
@endsection


