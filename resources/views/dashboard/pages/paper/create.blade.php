@extends('dashboard.layouts.master')
@section('content')
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-8">
<div class="card">
<div class="header">
<h4 class="title">Register a New Paper <!-- Trigger the modal with a button -->

</div>  
@include('dashboard.messenger.partials.flash')                      
<div class="content">
<form action ="{{url('/paper/new')}}" method="POST" enctype="multipart/form-data">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">                                 
      <div class="row">
        <div class="col-md-12">
            <div class="form-group  {{$errors->has('title')?'has-error':''}}">
                <label>Title*</label>
                <input type="text" class="form-control" placeholder="Paper Title" name="title" value="{{old('title')}}">
                @if($errors->has('title'))
                <span class="label label-danger">{{ $errors->first('title') }}
                </span>    
                @endif
            </div>
        </div>
    </div>

     <div class="row">
       <div class="col-md-6">
            <div class="form-group  {{$errors->has('paper_instructions')?'has-error':''}}">
                <label>Instructions*</label>                                
                <textarea name='paper_instructions' rows="5" placeholder="Paper Instructions here.." class="form-control">{{ old('paper_instructions') }}</textarea>
                @if($errors->has('paper_instructions'))
                <span class="label label-danger">{{ $errors->first('paper_instructions') }}
                </span>    
                @endif
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('author')?'has-error':''}}">
                <label>Paper Author</label>
                <select name = "author"class="form-control" />
                   <option value ="">----Select Author---</option>

                   @foreach($clients as $client)
                   <option value ="{{$client->id}}">{{$client->name}}</option>
                    @endforeach
                </select>
                @if($errors->has('author'))
                <span class="label label-danger">{{ $errors->first('author') }}
                </span>    
                @endif
            </div>
        </div>
    </div>  
     <div class="row">
          <div class="col-md-6">
            <div class="form-group  {{$errors->has('date_received')?'has-error':''}}">
                <label>Date Received</label>
                <input type="text" class="form-control" id="date_received" name="date_received" placeholder="Date Received" value="{{old('date_received')}}">
                @if($errors->has('date_received'))
                <span class="label label-danger">{{ $errors->first('date_received') }}
                </span>    
                @endif
            </div>
        </div>
         <div class="col-md-6">
            <div class="form-group  {{$errors->has('due_date')?'has-error':''}}">
                <label>Due Date</label>
                <input type="text" class="form-control" name="due_date" id="due_date" placeholder="Paper Due Date" value="{{old('due_date')}}">
                @if($errors->has('due_date'))
                <span class="label label-danger">{{ $errors->first('due_date') }}
                </span>    
                @endif
            </div>
        </div>
     </div>
        <div class="col-md-6">
            <div class="form-group  {{$errors->has('document')?'has-error':''}}">
                <label>Upload File</label>
                <input value="{{ old('document') }}"  type="file" name = "document"class="form-control" value="{{old('document')}}" />
                @if($errors->has('document'))
                <span class="label label-danger">{{ $errors->first('document') }}
                </span>    
                @endif
            </div>
        </div>
          <div class="col-md-6">
            <div class="form-group  {{$errors->has('number_of_pages')?'has-error':''}}">
                <label>Number of Pages</label>
                <input type="text" class="form-control" name="number_of_pages" placeholder="Number of Pages" value="{{old('number_of_pages')}}">
                @if($errors->has('number_of_pages'))
                <span class="label label-danger">{{ $errors->first('number_of_pages') }}
                </span>    
                @endif
            </div>
        </div>
     </div>
    <input type="submit" name='register' class="btn btn-success" value = "Register"/>
    <input type="reset" name='save' class="btn btn-default" value = "Cancel" />
    <div class="clearfix"></div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>



@endsection

@section('javascript')
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
 $(document).ready(function () {
  $('#due_date').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
         locale:{
            format: 'YYYY-MM-DD'
        }
    });
   $('#date_received').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        locale: {
            format: 'YYYY-MM-DD'
        }
    });
 });
</script>

<script type="text/javascript">
  $('select').select2();
</script>
@endsection