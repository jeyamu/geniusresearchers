@extends('dashboard.layouts.master')
@section('content')

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-9">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">User Management</h4>
                            </div>
                            @include('dashboard.messenger.partials.flash') 
                            <div class="content">
                                <ul class="nav nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#home">Admins</a></li>
                                    <li><a data-toggle="tab" href="#writers">Writers</a></li>
                                    <li><a data-toggle="tab" href="#clients">Clients</a></li>            
                                </ul>   
                                 <div class="tab-content">
                                        <div id="home" class="tab-pane fade in active">
                                            <h4>Admin's Details</h4>
                                             @if ( !$admins->count() )
                                            <div class="alert alert-warning">
                                                    <button type="button" aria-hidden="true" class="close">×</button>
                                                    <span>There are no Admins Added Yet</span>
                                                </div>                              
                                            @else                                
                                            <table class="display table table-hover" cellspacing="0" width="80%">
                                                <thead>  
                                                 <th>#</th>
                                                 <th>User</th>                                                  
                                                  <th>Gender</th> 
                                                <th>Phone</th>  
                                                <th>Date created</th>  
                                                <th></th>
                                                </thead>
                                                <tbody>
                                                <?php $count =1;?>
                                                @foreach($admins as $admin)
                                                 <tr>
                                                    <td>{{$count}}</td>
                                                    <td><a href="">{{$admin->name}}</a></td>                                                   
                                                    <td>{{$admin->gender}}</td>
                                                    <td>{{$admin->phone}}</td>
                                                    <td>{{$admin->created_at}}</td>  
                                                    @if(Auth::user()->role == 'Super Admin')  
                                                    <td><a href="{{route('edit-admin',$admin->id)}}"><i class="pe-7s-pen"></i></a></td>      
                                                    <td><a href="{{route('delete-admin',$admin->id)}}"><i class=" pe-7s-trash"></i></a></td>                                     
                                                   @endif
                                                </tr> 
                                                <?php $count++;?>
                                                @endforeach                                          
                                              
                                                </tbody>

                                            </table>
                                            {{ $admins->links() }}
                                          @endif
                                        </div><!--/Home-->

                                      <div id="writers" class="tab-pane fade">
                                            <h4>Writer's Details</h4>
                                             @if ( !$writers->count() )
                                            <div class="alert alert-warning">
                                                    <button type="button" aria-hidden="true" class="close">×</button>
                                                    <span>There are no Writers Added Yet</span>
                                                </div>                              
                                            @else                                
                                            <table class="display table table-hover" cellspacing="0" width="80%">
                                                <thead>  
                                                <th>#</th>
                                                <th>Name</th>                                                  
                                                <th>Writer Code</th> 
                                                <th>NIN</th> 
                                                <th>Skill Level</th>  
                                                <th>Qualification</th> 
                                                <th>Gender</th>   
                                                <th>Date created</th>  
                                                <th></th>
                                                </thead>
                                                <tbody>
                                                <?php $count =1;?>
                                                @foreach($writers as $writer)
                                                 <tr>
                                                    <td>{{$count}}</td>
                                                    <td><a href="{{route('show-writer',$writer->writer_code)}}">{{$writer->name}}</a></td>                                                   
                                                    <td>{{$writer->writer_code}}</td>
                                                    <td>{{$writer->nin}}</td>
                                                    <td>{{$writer->skill_level}}</td>
                                                    <td>{{$writer->qualification}}</td>
                                                    <td>{{$writer->gender}}</td>
                                                    <td><small>{{$writer->created_at}}</small></td>    
                                                    <td>
                                                    <a href="{{route('edit-writer',$writer->writer_code)}}"><i class="pe-7s-pen"></i></a></td>      
                                                     @if(Auth::user()->role == 'Super Admin')
                                                      <td>  <a href="{{route('delete-writer',$writer->writer_code)}}">
                                                       <i class="pe-7s-trash" data-toggle="tooltip" title="Delete Writer"></i></a>
                                                       </td>
                                                    @else
                                                    <td>
                                                      @if($writer->for_deletion == 2)
                                                       <a href="{{route('delete-writer',$writer->writer_code)}}">
                                                       <i class="pe-7s-trash" data-toggle="tooltip" title="Delete Writer"></i></a>
                                                       @elseif($writer->for_deletion == 0)
                                                         <a href="{{url('writer/submit/deletion/'.$writer->id)}}">
                                                       <i class="pe-7s-trash" data-toggle="tooltip" title="Submit Writer Deletion Request"></i></a>
                                                       @endif                                                    
                                                    </td>     
                                                    @endif                                                                      
                                              
                                                </tr> 
                                                <?php $count++;?>
                                                @endforeach                                          
                                              
                                                </tbody>

                                            </table>
                                            {{ $writers->links() }}
                                          @endif
                                        </div><!--/Writers-->

                                <div id="clients" class="tab-pane fade in">
                                            <h4>Client's Details</h4>
                                             @if ( !$clients->count() )
                                            <div class="alert alert-warning">
                                                    <button type="button" aria-hidden="true" class="close">×</button>
                                                    <span>There are no Clients Added Yet</span>
                                                </div>                              
                                            @else                                
                                            <table class="display table table-hover" cellspacing="0" width="80%">
                                                <thead>  
                                                <th>#</th>
                                                <th>Name</th>                                                  
                                                <th>Email</th> 
                                                <th>Phone</th>
                                                <th>Date Joined</th>   
                                                <th>Date created</th>  
                                                <th></th>
                                                </thead>
                                                <tbody>
                                                <?php $count =1;?>
                                                @foreach($clients as $client)
                                                 <tr>
                                                    <td>{{$count}}</td>
                                                    <td><a href="">{{$client->name}}</a></td> 
                                                    <td>{{$client->email}}</td> 
                                                     <td>{{$client->phone}}</td>                                                 
                                                    <td>{{$client->date_of_joining}}</td>
                                                    <td>{{$client->created_at}}</td> 
                                                    <td><a href="{{route('edit-client',$client->id)}}"><i class="pe-7s-pen"></i></a></td>      
                                                    @if(Auth::user()->role == 'Super Admin')
                                                      <td>  <a href="{{route('delete-client',$client->id)}}">
                                                       <i class="pe-7s-trash" data-toggle="tooltip" title="Delete Client"></i></a>
                                                       </td>
                                                    @else
                                                    <td>
                                                      @if($client->for_deletion == 2)
                                                       <a href="{{route('delete-client',$client->id)}}">
                                                       <i class="pe-7s-trash" data-toggle="tooltip" title="Delete Client"></i></a>
                                                       @elseif($client->for_deletion == 0)
                                                         <a href="{{url('client/submit/deletion/'.$client->id)}}">
                                                       <i class="pe-7s-trash" data-toggle="tooltip" title="Submit Deletion Request"></i></a>
                                                       @endif                                                    
                                                    </td>     
                                                    @endif                                        
                                                </tr> 
                                                <?php $count++;?>
                                                @endforeach                                          
                                              
                                                </tbody>

                                            </table>
                                            {{ $clients->links() }}
                                          @endif
                                        </div><!--/Clients-->                                             
                                         
                                        </div> <!--/Tab Content-->
                               
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                    <div class="text-center">
                                <h4>Quicklinks</h5>
                            </div>
                        <div class="card card-user">
                            <div class="content">
                              @if(Auth::user()->role =='Super Admin')
                             <div class="text-left">
                                <a href="{{route('create-admin')}}">Add New Admin</a>
                            </div>
                            @endif
                            <div class="text-left">
                                <a href="{{route('create-writer')}}"> Add New Writer</a>
                            </div>
                             <div class="text-left">
                                <a href="{{route('create-client')}}"> Add New Client</a>
                            </div>
                            @if(Auth::user()->role =='Super Admin')
                            <div class="text-left">
                                <a href="#" data-target="#approve_writer_deletion" data-toggle="modal"> Writer Deletion Requests</a>
                            </div>
                            <div class="text-left">
                                <a href="#" data-target="#approve_paper_deletion" data-toggle="modal"> Paper  Deletion Requests</a>
                            </div>
                            <div class="text-left">
                                <a href="#" data-target="#approve_client_deletion" data-toggle="modal"> Client Deletion Requests</a>
                            </div>
                            @endif
                               
                            </div>
                            <hr>
                           
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


<!-- Paper Approve Deletion -->
<div id="approve_paper_deletion" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Approve Paper deletions</h4>
      </div>
      <div class="modal-body">
      <?php
      $papers = App\Models\Paper::where('for_deletion',1)->get(['id','title']);
      ?>
        @if (count($papers) < 1)
            <div class="alert alert-warning">
                <strong>There are currently no papers to approve for deletion<br><br>
            </div>  
        @else 
      <table class="display table table-hover" cellspacing="0" width="80%">
        <thead> 
        <th>Paper</th>                                                  
        <th></th>
        </thead>
        <tbody>
        <?php $count =1;?>
        @foreach($papers as $data)
            <tr>
            <td>{{$data->title}}</td>    
            <td><a href="{{url('paper/approve/deletion/'.$data->id)}}">Approve Deletion</a></td>     
       
        </tr> 
        <?php $count++;?>
        @endforeach    
        </tbody>
    </table>       
    @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--/Paper Approve Deletion -->



<!-- Paper Approve Deletion -->
<div id="approve_writer_deletion" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Approve Writer deletions</h4>
      </div>
      <div class="modal-body">
      <?php
      $writers = App\Models\Writer::where('for_deletion',1)->get(['id','name','writer_code']);
      ?>
        @if (count($writers) < 1)
            <div class="alert alert-warning">
                <strong>There are currently no Writers to approve for deletion<br><br>
            </div>  
        @else 
      <table class="display table table-hover" cellspacing="0" width="80%">
        <thead> 
        <th>Writer</th> 
         <th>Code</th>                                                  
        <th></th>
        </thead>
        <tbody>
        <?php $count =1;?>
        @foreach($writers as $data)
            <tr>
            <td>{{$data->name}}</td> 
            <td>{{$data->writer_code}}</td>     
            <td><a href="{{url('writer/approve/deletion/'.$data->id)}}">Approve Deletion</a></td>     
            </tr> 
        <?php $count++;?>
        @endforeach    
        </tbody>
    </table>       
    @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--/Writer Approve Deletion -->


<!-- Cleint Approve Deletion -->
<div id="approve_client_deletion" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Approve Client deletions</h4>
      </div>
      <div class="modal-body">
      <?php
      $clients = App\Models\Client::where('for_deletion',1)->get(['id','name']);
      ?>
        @if (count($clients) < 1)
            <div class="alert alert-warning">
                <strong>There are currently no Clients to approve for deletion<br><br>
            </div>  
        @else 
      <table class="display table table-hover" cellspacing="0" width="80%">
        <thead> 
        <th>Client</th>                                                  
        <th></th>
        </thead>
        <tbody>
        <?php $count =1;?>
        @foreach($clients as $data)
            <tr>
            <td>{{$data->name}}</td>    
            <td><a href="{{url('client/approve/deletion/'.$data->id)}}">Approve Deletion</a></td>
            </tr> 
        <?php $count++;?>
        @endforeach    
        </tbody>
    </table>       
    @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<!--/Cleint Approve Deletion -->


@endsection
