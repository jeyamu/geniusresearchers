@extends('dashboard.layouts.master')
@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div class="card">
    <div class="header">
        <h4 class="title"> <a href="/messages/create"><input type="submit" name='save' class="btn btn-info" value = "Compose Message" /></a></h4>
    </div>
    <div class="content">
	@include('dashboard.messenger.partials.flash') 
<div class="">

	<div class="list-group">
		<div class="list-group-item">
			  <h1>{{ $thread->subject }}</h1>
        @each('dashboard.messenger.partials.messages', $thread->messages, 'message')
        @include('dashboard.messenger.partials.form-message')
		</div>
	</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
@endsection