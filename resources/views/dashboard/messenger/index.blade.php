@extends('dashboard.layouts.master')
@section('content')

<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-12">
<div class="card">
    <div class="header">
        <h4 class="title"> <a href="/messages/create"><input type="submit" name='save' class="btn btn-info" value = "Compose Message" /></a></h4>
    </div>
    <div class="content">
	@include('dashboard.messenger.partials.flash') 
<div class="row">

	<div class="list-group">
		<div class="list-group-item">
            @each('dashboard.messenger.partials.thread', $threads, 'thread', 'dashboard.messenger.partials.no-threads')
		</div>
	</div>
</div>
</div>
</div>
</div>

</div>
</div>
</div>
@endsection