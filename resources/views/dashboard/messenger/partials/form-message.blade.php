<form action="{{ route('messages.update', $thread->id) }}" method="post">
    {{ method_field('put') }}
    {{ csrf_field() }}
        
    <!-- Message Form Input -->
    <div class="row">
    <div class="form-group col-md-6">
        <textarea name="message" class="form-control">{{ old('message') }}</textarea>
    </div>
   <!-- Submit Form Input -->
    <div class="form-group col-md-6">
        <button type="submit" class="btn btn-info"><i class="pe-7s-back"></i>Reply</button>
    </div>
    </div>
</form>