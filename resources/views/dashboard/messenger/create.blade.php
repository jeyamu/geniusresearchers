@extends('dashboard.layouts.master')

@section('content')
<div class="content">
<div class="container-fluid">
<div class="row">
<div class="col-md-8">
<div class="card">
<div class="header">
<h4 class="title">Message</h4>
</div>            
@include('dashboard.messenger.partials.flash') 
    <form action="{{ route('messages.store') }}" method="post" name="messageForm">
        {{ csrf_field() }}
        <div class="row">
            <!-- Subject Form Input -->
            <div class="form-group col-md-6 {{$errors->has('subject')?'has-error':''}}">
                <label class="control-label">Subject</label>
                <input type="text" class="form-control" name="subject" placeholder="Subject"
                       value="{{ old('subject') }}">
                 @if($errors->has('subject'))
                <span class="label label-danger">{{ $errors->first('subject') }}
                </span>    
                @endif
            </div>
            </div>
            
            <div class="row">
             <!-- Message Form Input -->
            <div class="form-group col-md-6 {{$errors->has('message')?'has-error':''}}">
                <label class="control-label">Message</label>
                <textarea name="message" class="form-control">{{ old('message') }}</textarea>
                 @if($errors->has('message'))
                <span class="label label-danger">{{ $errors->first('message') }}
                </span>    
                @endif
            </div>
            </div>
            <div class="row">
            @if($users->count() > 0)
                 <div class="form-group col-md-6 {{$errors->has('recipients')?'has-error':''}}" >
                 <label class="control-label">Recipients</label><br>
                 <span class="label label-info">--Press Ctrl+Click to Select more participants</span>
                   <select id="participants" multiple="multiple" name="recipients[]" class="form-control">
                     @foreach($users as $user)
                        <option value="{{ $user->id }}">{{ $user->username }}</option>                                       value="{{ $user->id }}">{!!$user->username!!}</label>
                    @endforeach
                    </select>
                     @if($errors->has('recipients'))
                    <span class="label label-danger">{{ $errors->first('recipients') }}
                    </span>    
                    @endif                    
                </div>
            @endif
         </div>
       <input type="submit" name='register' class="btn btn-success" value = "SEND"/>
    <div class="clearfix"></div>
</form>
</div>
</div>
</div>
</div>
</div>
</div>
@endsection
