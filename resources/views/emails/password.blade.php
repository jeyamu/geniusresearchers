@extends('site.layouts.master')
    @section('content')
        <!-- Login section Start -->
        <div id="login" class="margin_top contact contact-us-section">
            <div class="container container-large">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-title text-center">
                            <h1>Reset Password</h1>
                       
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-xs-12">
                    	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading"><i class="fa fa-lock"></i>Reset Password</div>
				<div class="panel-body">
					Click here to reset your password: {{ url(\Request::getHost().'/password/reset/'.$token) }}			
				</div>
			     </div>
		       </div>
	           </div>                    
                            
                      
                    </div>
                </div>
            </div>
        </div>
        <!-- login section End -->
@endsection