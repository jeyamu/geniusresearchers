
<!-- Footer section Start -->
<div class="clearfix"></div>            
		<div class="">
			<div class="">			
					<div class="col-md-6 col-xs-12 col-md-pull-6">
						<p>Copyright ©  <?php echo Date('Y');?>  <span> <a href="#" target="_blank">Genius Researchers</a></span> ALL Rights Reserved.</p>
					</div>

			</div>
		</div>
		<div class="down-arrow">
			<a href="javascript:;" ><i class="fa fa-long-arrow-up" aria-hidden="true"></i></a>
		</div>
	</div>
	<!-- Footer section End -->
	<script src="{{asset('js/jquery.min.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/bootstrap.min.js')}}" type="text/javascript"></script>
</body>

</html>
