<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>
           @if(isset($title))
            {{$title}}
            @endif
        </title>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="image/fav-icon.png" type="image/png" sizes="16x16">
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css">
        <link rel="stylesheet" href="{{asset('css/responsive.css')}}" type="text/css">

    </head>
	<body>
<!-- header Start -->
<header id="top">
<div class="container-fluid">
<div class="row">                    
<div class="header fixed">
<div class="container container-large">
<div class="row">
<nav class="navbar top-menu">
<div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed btn_menu" data-toggle="collapse" data-target="#owo-toggle-navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
     <div class="row">
        </div>
</div><!-- /.container-fluid -->
</nav>
</header>
<!-- header End -->