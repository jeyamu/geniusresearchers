@extends('site.layouts.master')
@section('content')
        <!-- Login section Start -->
        <div id="login" class="margin_top contact contact-us-section">
            <div class="container container-large">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-title text-center">
                            <h1>Login</h1>
                            <p>Are you a Genius Researcher?.</p>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-xs-12">
                    	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading"><i class="fa fa-lock"></i>Login</div>
				<div class="panel-body">
				 @if (Session::has('error_message'))
				<div class="alert alert-danger" role="alert">
						{{ Session::get('error_message') }}
					</div>
				@elseif(Session::has('message'))
				<div class="alert alert-success" role="alert">
						{{ Session::get('message') }}
					</div>
				@elseif (count($errors) > 0)
				<div class="alert alert-danger">
					<strong>Whoops!</strong> There were some problems with your input.<br><br>
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
				@endif				            
					<form class="form-horizontal" role="form" method="POST" action="{{ route('login.auth') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">

						<div class="form-group">
							<label class="col-md-4 control-label">Username/Email</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="username" value="{{ old('username') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<div class="checkbox">
									<label>
										<input type="checkbox" name="remember"> Remember Me
									</label>
								</div>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Login</button>
                           
								<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>
							
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>                   
	<!-- login section End -->
@endsection