@extends('site.layouts.master')
    @section('content')
        <!-- Login section Start -->
        <div id="login" class="margin_top contact contact-us-section">
            <div class="container container-large">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box-title text-center">
                            <h1>Reset Password</h1>                       
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-xs-12">
                    	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">
				<div class="panel-heading"><i class="fa fa-lock"></i>Reset Password</div>
				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif					
                          
					<form class="form-horizontal" role="form" method="POST" action="/password/reset">
						  {!! csrf_field() !!}
						<input type="hidden" name="_token" value="{{$token}}">
						<div class="form-group">
							<label class="col-md-4 control-label">Email</label>
							<div class="col-md-6">
								<input type="text" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>		
						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>	
						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<button type="submit" class="btn btn-primary">Reset Password</button>
                           
							
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
                       
                            
                      
                    </div>
                </div>
            </div>
        </div>
        <!-- login section End -->
@endsection